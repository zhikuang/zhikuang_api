#!/usr/bin/env python
# coding=utf-8

from sqlalchemy import Column, String, Integer,Numeric,  DateTime, Boolean,Binary,TIMESTAMP,Float
from sqlalchemy import  ForeignKey, PrimaryKeyConstraint, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata

class CHDQUOTE(Base):
   __tablename__ = 'CHDQUOTE'

   date = Column(DateTime, nullable=False)
   exchange = Column(String(6), nullable=False)
   symbol = Column(String(8), nullable=False)
   code = Column(String(16))
   topen = Column(Numeric(9))
   tclose = Column(Numeric(9))
   high = Column(Numeric(9))
   low = Column(Numeric(9))
   volume = Column(Numeric(18))
   money = Column(Numeric(18))
   avg = Column(Numeric(9))
   pre_close = Column(Numeric(9))
   tmstamp = Column(Binary, nullable=False)
   paused = Column(Boolean)
   is_st = Column(Boolean)
   capitalization = Column(Float)
   circulating_cap = Column(Float)
   market_cap = Column(Float)
   circulating_market_cap = Column(Float)
   turnover_ratio = Column(Float)

   PK_CHDQUOTE = PrimaryKeyConstraint(date, exchange,symbol)


class IDCOMPT(Base):
    __tablename__ = 'idcompt'

    id = Column(Integer, autoincrement=True,unique = True, primary_key=True)
    iname = Column(String(200))
    icode = Column(String(8))
    code = Column(String(16))
    sname = Column(String(100))

    usestatus = Column(Binary)
    updatedate = Column(DateTime)
    culldate = Column(DateTime)
    culldeclaredate =Column(DateTime)


class SECURITIES(Base):
    __tablename__ = 'securities'

    code = Column(String(16),primary_key=True)
    display_name = Column(String(100), nullable=False)
    name = Column(String(20), nullable=False)
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    status = Column(Boolean)
    type = Column(String(20))


#市值数据
class VALUATION(Base):
    __tablename__ = 'valuation'

    ID = Column(Integer, autoincrement=True, primary_key=True)
    code = Column(String(16), nullable=False)
    reportdate = Column(DateTime, nullable=False)
    date = Column(DateTime, nullable=False)
    publishdate = Column(DateTime, nullable=False)
    capitalization = Column(Float)
    circulating_cap = Column(Float)
    market_cap = Column(Float)
    circulating_market_cap = Column(Float)
    turnover_ratio = Column(Float)
    pe_ratio = Column(Float)
    pb_ratio = Column(Float)
    ps_ratio = Column(Float)
    pcf_ratio = Column(Float)
    tmstamp = Column(TIMESTAMP)

    PK_valuation_1= UniqueConstraint(code,date,publishdate,reportdate)




#资产负债数据
class BALANCE(Base):
    __tablename__ = 'balance'

    code = Column(String(16),  nullable=False)
    date = Column(DateTime,  nullable=False)
    PublishDate = Column(DateTime, nullable=False)
    ReportDate = Column(DateTime,  nullable=False)
    cash_equivalents = Column(Float)
    settlement_provi = Column(Float)
    lend_capital = Column(Float)
    trading_assets = Column(Float)
    bill_receivable = Column(Float)
    account_receivable = Column(Float)
    advance_payment = Column(Float)
    insurance_receivables = Column(Float)
    reinsurance_receivables = Column(Float)
    reinsurance_contract_reserves_receivable = Column(Float)
    interest_receivable = Column(Float)
    total_assets = Column(Float)
    total_current_assets = Column(Float)
    other_receivable = Column(Float)
    bought_sellback_assets = Column(Float)
    inventories = Column(Float)
    non_current_asset_in_one_year = Column(Float)
    other_current_liability = Column(Float)
    total_current_liability = Column(Float)
    longterm_loan = Column(Float)
    bonds_payable = Column(Float)
    longterm_account_payable = Column(Float)
    specific_account_payable = Column(Float)
    estimate_liability = Column(Float)
    deferred_tax_liability = Column(Float)
    other_non_current_liability = Column(Float)
    total_non_current_liability = Column(Float)
    total_liability = Column(Float)
    paidin_capital = Column(Float)
    capital_reserve_fund = Column(Float)
    treasury_stock = Column(Float)
    specific_reserves = Column(Float)
    surplus_reserve_fund = Column(Float)
    ordinary_risk_reserve_fund = Column(Float)
    retained_profit = Column(Float)
    foreign_currency_report_conv_diff = Column(Float)
    equities_parent_company_owners = Column(Float)
    minority_interests = Column(Float)
    total_owner_equities = Column(Float)
    total_sheet_owner_equities = Column(Float)

    PK_income = PrimaryKeyConstraint(code, date,PublishDate, ReportDate)



#利润数据
class INCOME(Base):
   __tablename__ = 'income2'
   code = Column(String(16),ForeignKey("valuation.code"),nullable=False)
   date = Column(DateTime,ForeignKey("valuation.reportdate"), nullable=False)
   publishdate = Column(DateTime, nullable=False)
   reportdate =Column(DateTime,ForeignKey("valuation.reportdate"), nullable=False)
   total_operating_revenue = Column(Float)
   operating_revenue = Column(Float)
   interest_income = Column(Float)
   premiums_earned = Column(Float)
   commission_income = Column(Float)
   total_operating_cost = Column(Float)
   operating_cost = Column(Float)
   interest_expense = Column(Float)
   commission_expense = Column(Float)
   refunded_premiums = Column(Float)
   net_pay_insurance_claims = Column(Float)
   withdraw_insurance_contract_reserve = Column(Float)
   policy_dividend_payout = Column(Float)
   reinsurance_cost = Column(Float)
   operating_tax_surcharges = Column(Float)
   sale_expense = Column(Float)
   administration_expense = Column(Float)
   financial_expense = Column(Float)
   asset_impairment_loss = Column(Float)
   fair_value_variable_income = Column(Float)
   investment_income = Column(Float)
   invest_income_associates = Column(Float)
   exchange_income = Column(Float)
   operating_profit = Column(Float)
   non_operating_revenue = Column(Float)
   non_operating_expense = Column(Float)
   disposal_loss_non_current_liability = Column(Float)
   total_profit = Column(Float)
   income_tax_expense = Column(Float)
   net_profit = Column(Float)
   np_parent_company_owners = Column(Float)
   minority_profit = Column(Float)
   cinst60 = Column(Float)
   other_composite_income = Column(Float)
   total_composite_income = Column(Float)
   ci_parent_company_owners = Column(Float)
   ci_minority_owners  = Column(Float)
   cinst61 = Column(Float)
   basic_eps =  Column(Float)
   PK_income = PrimaryKeyConstraint(code, date,publishdate, reportdate)



class INDUSTRY_STOCKS(Base):
    __tablename__ = 'industry_stocks'

    id = Column(Integer, autoincrement=True,unique = True, primary_key=True)
    code = Column(String(16))
    stylecode = Column(String(100))
    industryname = Column(String(50))
    style = Column(String(10))

#财务指标数据
class INDICATTOR(Base):
   __tablename__ = 'indicator2'
   date = Column(DateTime,  nullable=False)
   code = Column(String(16),nullable=False)
   publishdate = Column(DateTime, nullable=False)
   reportdate =Column(DateTime, nullable=False)
   eps = Column(Float)
   adjusted_profit = Column(Float)
   operating_profit = Column(Float)
   value_change_profit = Column(Float)
   roe = Column(Float)
   inc_return = Column(Float)
   roa = Column(Float)
   net_profit_margin = Column(Float)
   gross_profit_margin = Column(Float)
   expense_to_total_revenue = Column(Float)
   operation_profit_to_total_revenue = Column(Float)
   net_profit_to_total_revenue = Column(Float)
   operating_expense_to_total_revenue = Column(Float)
   ga_expense_to_total_revenue = Column(Float)
   financing_expense_to_total_revenue = Column(Float)
   opeating_profit_profit = Column(Float)
   invesment_profit_to_profit = Column(Float)
   adjusted_profit_to_profit = Column(Float)
   goods_sale_and_service_to_revenue = Column(Float)
   ocf_to_revenue = Column(Float)
   ocf_to_operating_profit = Column(Float)
   inc_total_revenue_year_on_year = Column(Float)
   inc_total_revenue_anual = Column(Float)
   inc_revenue_year_on_year = Column(Float)
   inc_revenue_anual = Column(Float)
   inc_operation_profit_year_on_year = Column(Float)
   inc_operation_profit_annual = Column(Float)
   inc_net_profit_year_on_year = Column(Float)
   inc_net_profit_annual = Column(Float)
   inc_net_profit_to_shareholders_year = Column(Float)
   inc_net_profit_to_shareholders_annulal = Column(Float)
   PK_income = PrimaryKeyConstraint(code, date, publishdate, reportdate)

#现金流数据
class CASH_FLOW(Base):
   __tablename__ = 'cash_flow2'

   code = Column(String(16), nullable=False)
   date = Column(DateTime,  nullable=False)
   PublishDate = Column(DateTime, nullable=False)
   ReportDate = Column(DateTime, nullable=False)
   goods_sale_and_service_render_cash = Column(Float)
   net_deposit_increase = Column(Float)
   net_borrowing_from_central_bank = Column(Float)
   net_borrowing_from_finance_co = Column(Float)
   net_original_insurance_cash = Column(Float)
   net_cash_received_from_reinsurance_business = Column(Float)
   net_insurer_deposit_investment = Column(Float)
   net_deal_trading_assets = Column(Float)
   interest_and_commission_cashin = Column(Float)
   net_increase_in_placements = Column(Float)
   net_buyback = Column(Float)
   tax_levy_refund = Column(Float)
   other_cashin_related_operate = Column(Float)
   subtotal_operate_cash_inflow = Column(Float)
   goods_and_services_cash_paid = Column(Float)
   net_loan_and_advance_increase = Column(Float)
   net_deposit_in_cb_and_ib = Column(Float)
   original_compensation_paid = Column(Float)
   handling_charges_and_commission = Column(Float)
   policy_dividend_cash_paid = Column(Float)
   staff_behalf_paid = Column(Float)
   tax_payments = Column(Float)
   other_operate_cash_paid = Column(Float)
   subtotal_operate_cash_outflow = Column(Float)
   net_operate_cash_flow = Column(Float)
   invest_withdrawal_cash = Column(Float)
   invest_proceeds = Column(Float)
   fix_intan_other_asset_dispo_cas = Column(Float)
   net_cash_deal_subcompany = Column(Float)
   other_cash_from_invest_act = Column(Float)
   subtotal_invest_cash_inflow = Column(Float)
   fix_intan_other_asset_acqui_cash = Column(Float)
   invest_cash_paid = Column(Float)
   impawned_loan_net_increase = Column(Float)
   net_cash_from_sub_company = Column(Float)
   other_cash_to_invest_act = Column(Float)
   subtotal_invest_cash_outflow = Column(Float)
   net_invest_cash_flow = Column(Float)
   cash_from_invest = Column(Float)
   cash_from_mino_s_invest_sub = Column(Float)
   cash_from_borrowing = Column(Float)
   cash_from_bonds_issue = Column(Float)
   other_finance_act_cash = Column(Float)
   subtotal_finance_cash_inflow = Column(Float)
   borrowing_repayment = Column(Float)
   dividend_interest_payment = Column(Float)
   proceeds_from_sub_to_mino_s = Column(Float)
   other_finance_act_payment = Column(Float)
   subtotal_finance_cash_outflow = Column(Float)
   exchange_rate_change_effect = Column(Float)
   cash_equivalent_increase = Column(Float)
   cash_equivalents_at_beginning = Column(Float)
   cash_and_equivalents_at_end = Column(Float)
   PK_income = PrimaryKeyConstraint(code, date, PublishDate, ReportDate)