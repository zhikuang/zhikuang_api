import os
import sys
from sqlalchemy import func
sys.path.insert(0, os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
from zipline.api import *
from zhikuang_api.zk_get_data import *
import pandas as pd
from statsmodels.tsa.stattools import coint
import numpy as np


if __name__ == '__main__':
    # test
    #获取ORM query 对象 表对象列表，表及选择字段dict，所有选择字段列表
    #getfields(query(VALUATION))
    # df = get_fundamentals(query(
    #     VALUATION.code,func.avg(VALUATION.pe_ratio).label('pe_ratio')
    # ).filter(
    #     # VALUATION.date > '2015-01-01',
    #     # VALUATION.date < '2015-12-31',
    #     VALUATION.code.in_(['000651.SZ', '601318.SS', '000012.SZ', '002299.SZ', '600150.SS', '600027.SS',
    #                 '600900.SS'
    #     , '600406.SS', '600151.SS', '002241.SZ', '000563.SZ', '601668.SS', '002269.SZ'
    #     , '600019.SS', '600089.SS', '600362.SS', '002005.SZ', '600115.SS', '601866.SS', '600352.SS'
    #     , '600004.SS', '601186.SS', '002415.SZ', '002489.SZ', '002340.SZ', '601088.SS', '000581.SZ',
    #                 '000625.SZ'
    #     , '600917.SS', '600415.SS', '600196.SS', '600256.SS', '600887.SS', '600585.SS', '601006.SS',
    #                 '000063.SZ'
    #     , '300027.SZ', '600456.SS', '600511.SS', '600036.SS', '600519.SS', '600663.SS', '600030.SS',
    #                 '000538.SZ'
    #     , '600108.SS', '600655.SS'])
    #     # VALUATION.market_cap > 1000,
    #     # VALUATION.pe_ratio < 10,
    #     # INCOME.total_operating_revenue > 2e10
    # ).group_by(
    #     # 按市值降序排列
    #     VALUATION.code
    # ))
    # one_data = get_price('601166.SH', start_date='2008-10-10 00:00:00', end_date='2016-12-04 23:00:00', frequency='daily', fields=None)[
    #     'open']
    #
    # two_data = get_price('600000.SH', start_date='2008-10-10 00:00:00', end_date='2016-12-04 23:00:00',
    #                          frequency='daily', fields=['open', 'close'])
    # # score, pvalue, _ = coint(one_data.values, two_data.values)
    # print(type(one_data))
    # down_list = []
    # for i in one_data.index:
    #     print(one_data.index)
    #     print(two_data.index)
    #     if i in two_data.index:
    #         down_list.append(one_data[i]/two_data[i])
    # print(down_list)
    #
    # # 均值
    # mean = np.mean(down_list)
    # print(mean)

    h = attribute_history('600016.SH', 3, '1d', ('high', 'low', 'money'))
    print(h)
    print('-----------------------------------')

    #获取历史数据
    #data = get_price('000001.SZ')
    #data = get_price(['600602.SH'])
    #data = get_price(['600602.SH'],count= 10)
    #data = get_price(['600602.SH'], frequency= 'daily')
    #data = get_price(['600602.SH'],frequency='minute')
    #data = get_price(['600602.SH', '600603.SH', '600605.SH'],)['open'][:2]

    #data = get_price(get_index_stocks('000903.SZ')[:6])['open']
    #print(data)
    #data = get_price('000001.SZ')
    #data = get_price('000001.SZ', start_date='2015-01-01', end_date='2015-01-31 23:00:00',fields=['open', 'close'])
    #data = get_price('000001.SZ', count=2, end_date='2015-01-31', frequency='daily', fields=['open', 'close'])
    #data = get_price('000001.SZ', start_date='1991-01-01')

    #panel = get_price(get_index_stocks('000903.SZ')) # 获取中证100的所有成分股的2015年的天数据, 返回一个[pandas.Panel]
    #df_open = panel['open']  # 获取开盘价的[pandas.DataFrame],  行索引是[datetime.datetime]对象, 列索引是股票代号
    #df_volume = panel['volume']  # 获取交易量的[pandas.DataFrame]
    #print(df_open['000001.SZ'])

    #获取历史数据
    # his = history(5, security_list=['000001.SZ', '000002.SZ'],field= 'low' )
    # print(his)
    #h = attribute_history('000001.SZ', count=2,df = False)
    #print(h)
    #current_data = get_current_data()
    #print(current_data['000001.SZ'].paused)
    #print(current_data['000001.SZ'])

    # 获取单个标的信息
    #df = get_security_info('000789.SZ')
    #print(df)
    #获取基金净值/期货结算价等
    #data = get_extras('isxs_st', ['600601.SH', '600602.SH', '600652.SH'], start_date='1990-12-21',end_date='1990-12-23', df=False)
    #data = get_extras('is_st', ['600601.SH', '600602.SH', '600652.SH'], start_date='1990-12-21',end_date='1990-12-23', df=True)
    #data =get_extras('is_st', ['600601.SH', '600602.SH', '600652.SH'], start_date='2006-12-21', end_date='2006-12-23',df = False)
    #data = get_extras('acc_net_value', ['510300.SZ', '510050.SZ'], start_date='2015-12-01', end_date='2015-12-03')

    #print(df)

    # 查询'600000.SH'的所有市值数据, 时间是2005-06-30  #VALUATION
    '''q = query(CHDQUOTE).filter(CHDQUOTE.code == '600000.SH')
    df = get_fundamentals(q, '2005-06-20')
    print(df['market_cap'][0])'''

    '''q = query(CHDQUOTE.code, CHDQUOTE.market_cap).filter(CHDQUOTE.code == '600000.SH')
    df = get_fundamentals(q, '2005-06-30')
    print(df)'''

    # 获取多只股票在某一日期的市值, 利润#VALUATION
    '''df = get_fundamentals(query(
        CHDQUOTE , INCOME
    ).filter(
        CHDQUOTE.code.in_(['600010.SH','600011.SH'])
    ), date='2016-10-1')
    print(df)'''

    '''df = get_fundamentals(query(
        CHDQUOTE, INCOME
    ).filter(
        CHDQUOTE.code.in_(['600010.SH', '600011.SH'])
    ), date='2016-11-20')
    print(df)'''

    '''df = get_fundamentals(query(
        CHDQUOTE, INCOME
    ).filter(
        CHDQUOTE.code.in_(['600010.SH', '600011.SH'])
    ), date='2015-10-15')
    print(df)'''
    # 选出所有的总市值大于1000亿元, 市盈率小于10, 营业总收入大于200亿元的股票 ---待于雷添加
    '''df = get_fundamentals(query(
        VALUATION.code, VALUATION.market_cap, VALUATION.pe_ratio, INCOME.total_operating_revenue
    ).filter(
        VALUATION.market_cap > 1000,
        VALUATION.pe_ratio < 10,
        INCOME.total_operating_revenue > 2e10
    ).order_by(
        # 按市值降序排列
        VALUATION.market_cap.desc()
    ).limit(
        # 最多返回100个
        100
    ), date='2015-10-15')
    print(df)'''

    #使用 or_ 函数: 查询总市值大于1000亿元 **或者** 市盈率小于10的股票 ---待于雷添加
    '''print(get_fundamentals(query(
        VALUATION.code
    ).filter(
        or_(
            VALUATION.market_cap > 1000,
            VALUATION.pe_ratio  < 10
        )
    ),date = '2015-12-31'))'''

    # 查询平安银行2014年四个季度的季报, 放到数组中
    '''q = query(
        INCOME.date,
        INCOME.code,
        INCOME.basic_eps,
        BALANCE.cash_equivalents,
        CASH_FLOW.goods_sale_and_service_render_cash
    ).filter(
        INCOME.code == '000001.SZ',
    )

    rets = [get_fundamentals(q, statDate='2014Q' + str(i)) for i in range(1,5)]
    print(rets)'''

    # 查询平安银行2014年的年报
    '''q = query(
        INCOME.date,
        INCOME.code,
        INCOME.cinst60,
        CASH_FLOW.goods_sale_and_service_render_cash
    ).filter(
        INCOME.code == '000001.SZ',
    )

    ret = get_fundamentals(q, statDate='2014')
    print(ret)'''
    # 获取指数成份股
    #stocks = get_index_stocks('000903.SZ')
    #print(stocks)
    #data = get_index_stocks('000300.sz')
    #print(data)

    # 获取概念成份股
    #stocks = get_concept_stocks(698)
    #print(stocks)



    # 获取所有标的信息
    #df = get_all_securities()[:2]

    #print(df)


    #print(get_industry_stocks('I64'))
    #dt = convert_datetime('2017-1-1')

    #arr = get_all_trade_days(dt)
    '''print("获取2016-12-20到2017-1-1所有交易日")
    trade_days = get_trade_days(start_date='2016-12-20', end_date='2017-1-1')
    print(trade_days)
    print("获取2017-2-1前5个交易日")
    trade_days = get_trade_days(end_date='2017-2-1', count=5)
    print(trade_days)'''
