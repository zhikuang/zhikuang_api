import logging
from celery import Celery
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
#sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))
sys.path.insert(0,'/home/chenxj/projects/squant_tools/squant/worker')

import django
from django.conf import settings
from quant.conf import DB_SETTING
import datetime
import pytz
from contextlib import ExitStack
from squant.worker.calculator import cal_all_data_for_trade
from logbook.ticketing import TicketingHandler

import subprocess
import platform
from cn_stock_holidays.data import is_trading_day, previous_trading_day

import pandas as pd
import re
from squant import constants
from squant.worker.queue_config import task_routes
import getpass

from squant.zipline.blotter import Blotter
from squant.zipline.finance.commission import OrderCost
from squant.zipline.finance.tracker import PerformanceTracker
from pprint import pprint
import inspect
import pickle
from contextlib import contextmanager
import zipline.api
from zipline.utils.algo_instance import get_algo_instance, set_algo_instance
from zhikuang_api.zk_get_data  import get_price ,history,attribute_history,get_current_data 



bundle = 'cn_squant'

logging.basicConfig(level=logging.INFO)
app = Celery('daily_trade_worker', broker='redis://localhost:6379/0')
app.conf.task_routes = task_routes


print("__name__ is : %s " % __name__)

# 防止重复初始化
if not settings.configured:
    settings.configure(
        DATABASES={
            'default': DB_SETTING,
        },
        INSTALLED_APPS=('webapp',)
    )
    django.setup()

from webapp.models import SimulationTrade, TradeState, SimulationTradeContext
from django.db import connection

BTS_TIME = (8, 30) # 开盘之前一个小时
DAILY_BAR_TIME = (15, 00) # 股市收盘时间

# 判断程序是直接由celery 调用的
from zipline.protocol import BarData
from zipline.utils.api_support import ZiplineAPI
from zipline.data.bundles import register
from zipline.algorithm import TradingAlgorithm
from squant.zipline.quant_algorithm import QuantAlgorithm
from zipline.finance.trading import TradingEnvironment
from zipline.finance import trading
from zipline.utils.factory import create_simulation_parameters
from zipline.data.bundles.core import load
from zipline.data.data_portal import DataPortal
from zipline_cn_databundle.loader import load_market_data
from zipline_cn_databundle.squant_source import squant_bundle
from cn_stock_holidays.zipline.default_calendar import shsz_calendar
from squant.zipline.squantlogger import QuantLogBackend

def daily_bar(trade_id):
    try:
        trade = SimulationTrade.objects.get(pk=trade_id)
    except SimulationTrade.DoesNotExists as e:
        logging.warning (str(e))

    # handler = TicketingHandler(
    #     'mysql+mysqldb://' + DB_SETTING['USER'] + ':' + DB_SETTING['PASSWORD'] + '@' + DB_SETTING['HOST'] + '/' +
    #     DB_SETTING['NAME'] + '?charset=utf8', backend=QuantLogBackend,
    #     **{'uid': trade.uid, 'related_id': trade.id, 'related_type': 1})# related_type: 0 - 回测 ，1 - 交易
    #
    # with handler.applicationbound():#
    algor_obj, data_portal, assets = build_trading_algorithm(trade, False)
    today = datetime.date.today()
    dt = pytz.timezone("Asia/Shanghai").localize(datetime.datetime.combine(today, datetime.time(*DAILY_BAR_TIME))).astimezone(pytz.utc)

    def on_exit():
        nonlocal algor_obj, data_portal, assets
        algor_obj = data_portal = assets= None
        if connection.connection is not None:
            connection.connection.close()
            connection.connection = None

    with ExitStack() as stack:
        stack.callback(on_exit)
        stack.enter_context(ZiplineAPI(algor_obj))
        algor_obj.on_dt_changed(dt)

        def _universe_func():
            return assets
        # 新版的zipline应该包含一个 restriction 字段
        bar_data = BarData(
            data_portal=data_portal,
            simulation_dt_func=dt,
            data_frequency="daily",
            trading_calendar=shsz_calendar,
            universe_func=_universe_func
        )

        algor_obj.event_manager.handle_data(algor_obj, bar_data, dt)



def load_b_and_t():
    return load_market_data(shsz_calendar.day, shsz_calendar.schedule.index, bm_symbol=constants.DEFAULT_BENCHMARK_TICKER, trading_day_before=0)

def daily_calculate(trade_id):
    return cal_all_data_for_trade(trade_id)

def build_trading_algorithm(trade, today_ingested=False):
    backtest = trade.backtest
    algo_script = backtest.sourcecode
    capital_base = trade.capital_base

    start_date = trade.start_at.strftime('%Y-%m-%d')

    if today_ingested:
        end_date = datetime.datetime.now().strftime('%Y-%m-%d')
    else:
        end_date = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')

    if start_date > end_date:
        start_date = end_date

    register(
        bundle,
        squant_bundle,
        "SHSZ",
        pd.Timestamp(start_date, tz='utc'),
        pd.Timestamp(end_date, tz='utc')
    )

    bundle_data = load(
        bundle,
        os.environ,
        None,
    )

    prefix, connstr = re.split(
        r'sqlite:///',
        str(bundle_data.asset_finder.engine.url),
        maxsplit=1,
    )

    env = trading.environment = TradingEnvironment(asset_db_path=connstr,
                                                   trading_calendar=shsz_calendar,
                                                   bm_symbol=constants.DEFAULT_BENCHMARK_TICKER,
                                                   load=load_market_data)

    first_trading_day = \
        bundle_data.equity_minute_bar_reader.first_trading_day
    data = DataPortal(
        env.asset_finder, shsz_calendar,
        first_trading_day=first_trading_day,
        equity_minute_reader=bundle_data.equity_minute_bar_reader,
        equity_daily_reader=bundle_data.equity_daily_bar_reader,
        adjustment_reader=bundle_data.adjustment_reader,
    )

    sim_params = create_simulation_parameters(
        start=pd.to_datetime(start_date + " 00:00:00").tz_localize("Asia/Shanghai"),
        end=pd.to_datetime(end_date + " 00:00:00").tz_localize("Asia/Shanghai"),
        data_frequency="daily", emission_rate="daily", trading_calendar=shsz_calendar)

    algor_obj = QuantAlgorithm(script=algo_script,
                                 sim_params=sim_params,
                                 env=trading.environment,
                                 trading_calendar=shsz_calendar,
                                 capital_base=capital_base)
    algor_obj.set_commission(OrderCost())
    algor_obj.data_portal = data
    assets = algor_obj._assets_from_source = \
        algor_obj.trading_environment.asset_finder.retrieve_all(
            algor_obj.trading_environment.asset_finder.sids
        )
    algor_obj.perf_tracker = None


    return algor_obj, data, assets

class LiveAlgorithmSimulator:
    EMISSION_TO_PERF_KEY_MAP = {
        'minute': 'minute_perf',
        'daily': 'daily_perf'
    }

    def __init__(self, algo, sim_params, data_portal, clock, benchmark_source,
                 restrictions, universe_func, simulation_dt):
        # ==============
        # Simulation
        # Param Setup
        # ==============
        self.sim_params = sim_params
        self.env = algo.trading_environment
        self.data_portal = data_portal
        self.restrictions = restrictions

        # ==============
        # Algo Setup
        # ==============
        self.algo = algo

        # ==============
        # Snapshot Setup
        # ==============

        # This object is the way that user algorithms interact with OHLCV data,
        # fetcher data, and some API methods like `data.can_trade`.
        self.current_data = self._create_bar_data(universe_func)

        # We don't have a datetime for the current snapshot until we
        # receive a message.
        self.simulation_dt = simulation_dt
        self.clock = clock
        self.benchmark_source = benchmark_source

    def _create_bar_data(self, universe_func):
        return BarData(
            data_portal=self.data_portal,
            simulation_dt_func=self.get_simulation_dt,
            data_frequency=self.sim_params.data_frequency,
            trading_calendar=self.algo.trading_calendar,
           # restrictions=self.restrictions,
            universe_func=universe_func,
        )

    def get_simulation_dt(self):
        return self.simulation_dt




    def handle_data(self):

        algor_obj = self.algo
        data_portal = self.data_portal

        def on_exit():
            nonlocal algor_obj, data_portal
            algor_obj = data_portal = None
            if connection.connection is not None:
                connection.connection.close()
                connection.connection = None

        with ExitStack() as stack:
            stack.callback(on_exit)
            stack.enter_context(ZiplineAPI(self.algo))
            algor_obj.on_dt_changed(self.simulation_dt)

            # 新版的zipline应该包含一个 restriction 字段
            bar_data = BarData(
                data_portal=data_portal,
                simulation_dt_func=self.get_simulation_dt,
                data_frequency="daily",
                trading_calendar=shsz_calendar,
                universe_func=algor_obj._calculate_universe
            )
            # pprint( ( inspect.getsourcelines(algor_obj.event_manager._events[0].callback)))
            algor_obj.event_manager.handle_data(algor_obj, bar_data, self.simulation_dt)



def test_daily_bar(trade_id):
    from squant.zipline.finance.tracker import PerformanceTracker
    try:
        trade = SimulationTrade.objects.get(pk=trade_id)
    except SimulationTrade.DoesNotExists as e:
        logging.warning(str(e))

    algor_obj, data_portal, assets = build_trading_algorithm(trade, False)

    algor_obj.blotter = Blotter(data_frequency=None, asset_finder=None, trade=trade)
    algor_obj.perf_tracker = PerformanceTracker(sim_params=algor_obj.sim_params, trading_calendar=shsz_calendar,
                                                env=algor_obj.trading_environment,
                                                trade=trade)

    trade_day = datetime.date(2017, 2, 19)
    dt = pytz.timezone("Asia/Shanghai").localize(
        datetime.datetime.combine(trade_day, datetime.time(*DAILY_BAR_TIME))).astimezone(pytz.utc)
    pd_dt = pd.to_datetime(dt)

    if not algor_obj.initialized:
        algor_obj.initialize()
        algor_obj.initialized = True

    algor_obj.trading_client = LiveAlgorithmSimulator(
        algor_obj,
        algor_obj.sim_params,
        algor_obj.data_portal,
        algor_obj._create_clock(),
        algor_obj._create_benchmark_source(),
        None,
        universe_func=algor_obj._calculate_universe,
        simulation_dt=pd_dt
    )
    print(dir(algor_obj))
    with keep_algo_context(trade, algor_obj):
        algor_obj.trading_client.handle_data()
    # pickle.dump(algor_obj, open("/tmp/algor_obj.pickle", "w"))
    return algor_obj


@contextmanager
def keep_algo_context(trade, algor_obj):
    """
    保存算法类对象的状态到数据库中
    :param trade: simluation trade对象
    :param algor_obj: 算法类对象
    :return:
    """
    SimulationTradeContext.load_context(trade, algor_obj)
    yield
    SimulationTradeContext.save_context(trade, algor_obj)




class FakePerfTracker:

    def set_date(self, dt):
        pass

if __name__ == '__main__':

    # do_zipline_ingest()
    algo = test_daily_bar(13)
    #df= get_price('000001.SZ')
    #print(df)
    set_algo_instance(algo)
    #algo.simulation_dt = '2007-11-11'
    algo.on_dt_changed('2015-01-07')

    #h = history(5, security_list=['000001.SZ', '000002.SZ'],df = False)
    #print(h)
    #stock = '000001.SZ'
    #h = attribute_history(stock, 5, '1d', ('open','close', 'volume'),df = False)

    current_data = get_current_data()
    print(type(current_data))
    print(type(current_data['000001.SZ']))
    print(current_data['000001.SZ'].paused)
    print(current_data['000001.SZ'].open)
