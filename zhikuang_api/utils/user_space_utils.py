﻿#!/usr/bin/env python
#-*- coding: utf-8 -*-

# all common third-party modules
import os,re,sys,json




bIsLinux = sys.platform.startswith('linux')
bIsWindows = sys.platform.startswith('win32')
bIsMac = sys.platform.startswith('darwin')


bInUserspace = 'ZHIKUANG_USER_SPACE' in os.environ
bInBackTest  = 'ZHIKUANG_USER_BACK_TEST' in os.environ

if __name__ == '__main__':
    #print(bIsLinux)
    #print(bIsWindows)
    #print(bIsMac)
    #print(bInUserspace)
    print(bInBackTest)