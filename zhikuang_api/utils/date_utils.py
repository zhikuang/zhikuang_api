#!/usr/bin/env python
# coding=utf-8

import datetime,time,calendar


#获取所有天，返回一个过去的日期列表
def getPastDays(begin_date = None,end_date = None):
    """
    :param begin_date: 日期，默认是‘2015-01-01’，向后截止日期本今天
    :param end_date: 日期，默认是现在,如：'2017-06-11'

    :return: 日期列表   ，[datetime.date类型]
    """
    date_list = []
    if(begin_date is None):
        begin_date = '2015-01-01'
    begin_date = datetime.datetime.strptime(begin_date, "%Y-%m-%d")
    if(end_date is None):
        end_date = datetime.datetime.strptime(time.strftime('%Y-%m-%d', time.localtime(time.time())), "%Y-%m-%d")
    if(isinstance(end_date,str)):
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    while begin_date <= end_date:
        date_str = begin_date.strftime("%Y-%m-%d")
        date_list.append(date_str)
        begin_date += datetime.timedelta(days=1)
    return date_list

#获取所有天，返回一个向前历史列表
def getHisDays(begin_date):
    """
    :param date: 日期，默认是‘2015-01-01’， 
    :return: 日期列表 ，[datetime.date类型]，向前截止日期：2004-01-01
    """
    date_list = []
    if (begin_date == None):
        begin_date = '2015-01-01'
    begin_date = datetime.datetime.strptime(begin_date, "%Y-%m-%d")

    forward_date = datetime.datetime.strptime('2004-01-01', "%Y-%m-%d")
    while begin_date >= forward_date:
        date_str = begin_date.strftime("%Y-%m-%d")
        date_list.append(date_str)
        begin_date -= datetime.timedelta(days=1)
    return date_list




def add_months(dt, months):
    month = dt.month - 1 + months
    year = int(dt.year + month / 12)
    month = month % 12 + 1

    day = min(dt.day, calendar.monthrange(year, month)[1])
    return dt.replace(year=year, month=month, day=day)

def Subtract_months(dt, months):
    month = dt.month - 1 - months
    year = int(dt.year + month / 12)
    month = month % 12 + 1

    day = min(dt.day, calendar.monthrange(year, month)[1])
    return dt.replace(year=year, month=month, day=day)

#获取所有月，返回一个向后列表Pasts
def getPastMonths(begin_date):
    """
    :param date: 日期，默认是‘2015-01-01’， 
    :return: 月列表 ，[‘200401’，‘201402’，..] 截止到本月
    """
    date_list = []
    if (begin_date == None):
        begin_date = '2015-01-01'
    begin_date = datetime.datetime.strptime(begin_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(time.strftime('%Y-%m-%d', time.localtime(time.time())), "%Y-%m-%d")

    while begin_date <= end_date:
        date_str = begin_date.strftime("%Y%m")
        date_list.append(date_str)
        begin_date = add_months(begin_date, 1)
    return date_list

#获取所有月，返回一个向前列表Pasts
def getHisMonths(begin_date):
    """
    :param date: 日期，默认是‘2015-01-01’， 
    :return: 月列表 ，[‘201511’，‘201510’，..] 截止到2004-01
    """
    date_list = []
    if (begin_date == None):
        begin_date = '2015-01-01'
    begin_date = datetime.datetime.strptime(begin_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime('2004-01-01', "%Y-%m-%d")

    while begin_date >= end_date:
        date_str = begin_date.strftime("%Y%m")
        date_list.append(date_str)
        begin_date = Subtract_months(begin_date, 1)
    return date_list


#获取所有季度，返回一个列表:
def getPastQuarter(begin_date ):
    """
    :param date: 日期，默认是‘2015-01-01’， 
    :return: 月列表 ，[‘201511’，‘201510’，..] 截止到2004-01
    """
    quarter_list = []
    month_list = getPastMonths(begin_date)
    for value in month_list:
        #tempvalue = value.split("-")
        tempvalue = [value[:4],value[4:] ]
        if tempvalue[1] in ['01','02','03']:
            quarter_list.append(tempvalue[0] + "Q1")
        elif tempvalue[1] in ['04','05','06']:
            quarter_list.append(tempvalue[0] + "Q2")
        elif tempvalue[1] in ['07', '08', '09']:
            quarter_list.append(tempvalue[0] + "Q3")
        elif tempvalue[1] in ['10', '11', '12']:
            quarter_list.append(tempvalue[0] + "Q4")
    quarter_set = set(quarter_list)
    quarter_list = list(quarter_set)
    quarter_list.sort()
    return quarter_list

#获取所有季度，返回一个列表:
def getHisQuarter(begin_date ):
    """
    :param date: 日期，默认是‘2015-01-01’， 
    :return: 月列表 ，[‘201511’，‘201510’，..] 截止到2004-01
    """
    quarter_list = []
    month_list = getHisMonths((begin_date))
    for value in month_list:
        #tempvalue = value.split("-")
        tempvalue = [value[:4],value[4:] ]
        if tempvalue[1] in ['01','02','03']:
            quarter_list.append(tempvalue[0] + "Q1")
        elif tempvalue[1] in ['04','05','06']:
            quarter_list.append(tempvalue[0] + "Q2")
        elif tempvalue[1] in ['07', '08', '09']:
            quarter_list.append(tempvalue[0] + "Q3")
        elif tempvalue[1] in ['10', '11', '12']:
            quarter_list.append(tempvalue[0] + "Q4")
    quarter_set = set(quarter_list)
    quarter_list = list(quarter_set)
    quarter_list.sort(reverse=True)
    return quarter_list

#获取指定月最后一天
def getMonthLastDay(year=None, month=None):
    """
    :param year: 年份，默认是本年，可传int或str类型
    :param month: 月份，默认是本月，可传int或str类型
    :return: lastDay: 当月的最后一天，datetime.date类型
    """
    if year:
        year = int(year)
    else:
        year = datetime.date.today().year

    if month:
        month = int(month)
    else:
        month = datetime.date.today().month

    # 获取当月第一天的星期和当月的总天数
    firstDayWeekDay, monthRange = calendar.monthrange(year, month)

    # 获取当月的第一天
    #firstDay = datetime.date(year=year, month=month, day=1)
    lastDay = datetime.date(year=year, month=month, day=monthRange)

    return  lastDay

def getLastQuarterEndDays(begin_date):
    """
    :param begin_date: 日期，默认是‘2015-01-01’，  
    :return: [datetime1,datetime2,... )]
    """
    month = getPastMonths(begin_date)[0]

    quarterlist = getHisQuarter(begin_date)
    begin_date = datetime.datetime.strptime(begin_date, "%Y-%m-%d")
    myday = [getMonthLastDay(int(x[0:4]),3*int(x[5:])) for x in quarterlist]
    if(myday):
        if(begin_date.date() <myday[0]):
            del myday[0]
    return myday

#日期转换
def date_to_datetime(dt):
    """
       :param : 日期，支持格式 字符串‘2015-01-01’，datetime.datetime，datetime.datetime
       :return: datetime.datetime 
    """
    if isinstance(dt, str):
        if ':' in dt:
            return datetime.datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
        else:
            return datetime.datetime.strptime(dt, '%Y-%m-%d')
    elif isinstance(dt, datetime.datetime):
        return dt
    elif isinstance(dt, datetime.date):
        return datetime.datetime.combine(dt, datetime.time.min)
    raise Exception("date 必须是datetime.date, datetime.datetime或者如下格式的字符串:'2015-01-05'")

def convert_date(date):
    """将日期转换为date格式
           :param : 日期，支持格式 字符串‘2015-01-01’，'2015-01-01 00:00:00'，datetime.datetime,datetime.date
           :return: datetime.date  
    """

    if isinstance(date,str):
        if ':' in date:
            date = date[:10]
        return datetime.datetime.strptime(date, '%Y-%m-%d').date()
    elif isinstance(date, datetime.datetime):
        return date.date()
    elif isinstance(date, datetime.date):
        return date
    raise Exception("date 支持格式：‘2015-01-01’，'2015-01-01 00:00:00'，datetime.datetime,datetime.date")
    pass

# convert anything to datetime.datetime
def convert_datetime(dt):
    """将日期转换为datetime.datetime格式
              :param : 日期，支持格式 字符串‘2015-01-01’，'2015-01-01 00:00:00'，datetime.datetime,datetime.date
              :return: datetime.datetime  
    """
    print(dt)
    if isinstance(dt,str):
        if ':' in dt:
            return datetime.datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
        else:
            return datetime.datetime.strptime(dt, '%Y-%m-%d')
    elif isinstance(dt, datetime.datetime):
        return dt
    elif isinstance(dt, datetime.date):
        return datetime.datetime.combine(dt, datetime.time.min)
    raise Exception("date 支持格式：‘2015-01-01’，'2015-01-01 00:00:00'，datetime.datetime,datetime.date")


if __name__ == '__main__':
    print(date_to_datetime('2014-11-11'))
