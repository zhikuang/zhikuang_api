# 智矿获取数据API
## get_price - 获取历史数据

```python
get_price(security, start_date=None, end_date=None, frequency='daily', fields=None, skip_paused=False, fq='pre', count=None)
```
获取一支或者多只股票的行情数据, 按天或者按分钟，这里请在使用时注意防止未来函数
SecurityUnitData关于停牌: 因为此API可以获取多只股票的数据, 可能有的股票停牌有的没有, 为了保持时间轴的一致, 我们默认没有跳过停牌的日期, 停牌时使用停牌前的数据填充(请看  的 paused 属性). 如想跳过, 请使用 skip_paused=True 参数, 同时只取一只股票的信息

**参数**
- security: 一支股票代码或者一个股票代码的list, 如 ['000001.SZ','600601.SH'] 或‘000001.SZ,600601.SH’
- count: 与 start_date 二选一，不可同时使用. 数量, 返回的结果集的行数, 即表示获取 end_date 之前几个 frequency 的数据
- start_date: 与 count 二选一，不可同时使用. 字符串或者 datetime.datetime/datetime.date 对象, 开始时间. 
  
    - 如果 count 和 start_date 参数都没有, 则 start_date 生效, 值是 ‘2015-01-01’.
    - 当取分钟数据时, 时间可以精确到分钟, 比如: 传入 datetime.datetime(2015, 1, 1, 10, 0, 0) 或者 '2015-01-01 10:00:00'.
    - 当取分钟数据时, 如果只传入日期, 则日内时间是当日的 00:00:00.
    - 当取天数据时, 传入的日内时间会被忽略
- end_date: 格式同上, 结束时间, 默认是’2015-12-31’, 包含此日期. 注意: 当取分钟数据时, 如果 end_date 只有日期, 则日内时间等同于 00:00:00, 所以返回的数据是不包括 end_date 这一天的.
- frequency: 单位时间长度, 几天或者几分钟, 现在支持’Xd’,’Xm’, ‘daily’(等同于’1d’), ‘minute’(等同于’1m’), X是一个正整数, 分别表示X天和X分钟(不论是按天还是按分钟回测都能拿到这两种单位的数据), 注意, 当X > 1时, fields只支持[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段. 默认值是daily

- fields: 字符串list, 选择要获取的行情数据字段, 默认是None(表示[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段), 支持SecurityUnitData里面的所有基本属性,，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’, ‘factor’, ‘high_limit’,’ low_limit’, ‘avg’, ’ pre_close’, ‘paused’]

- skip_paused: 是否跳过不交易日期(包括停牌, 未上市或者退市后的日期). 如果不跳过, 停牌时会使用停牌前的数据填充(具体请看SecurityUnitData的paused属性), 上市前或者退市后数据都为 nan, 但要注意:

    - 默认为 False
    - 当 skip_paused 是 True 时, 只能取一只股票的信息
 
**返回**

- 请注意, 为了方便比较一只股票的多个属性, 同时也满足对比多只股票的一个属性的需求, 我们在security参数是一只股票和多只股票时返回的结构完全不一样

- 如果是一支股票, 则返回pandas.DataFrame对象, 行索引是datetime.datetime对象, 列索引是行情字段名字, 比如’open’/’close’. 比如: 
```python
get_price('600602.SH')[:2]
```
**返回**

|          |    open  |     close  |     high   |     low    |   volume   |        money 
|----------|------------|------------|------------|------------|------------------|-------------------|
|2015-01-05|8.4600000000|8.6400000000|8.7600000000|8.4100000000|3373544.0000000000|28943185.0000000000|
|2015-01-06|8.5800000000|8.6700000000|8.6900000000|8.4600000000|2904789.0000000000|24855638.0000000000|

- 如果是多支股票, 则返回pandas.Panel对象, 里面是很多pandas.DataFrame对象, 索引是行情字段(open/close/…), 每个pandas.DataFrame的行索引是datetime.datetime对象, 列索引是股票代号. 比如
```python20.5801000000
data = get_price(['600602.SH', '600603.SH', '600605.SH'],)['open'][:2]
```
**返回**
|          |  600602.SH  | 600603.SH  |  600605.SH  |
|----------|-------------|------------|-------------|
|2015-01-20| 6.3400000000|8.2200000000|12.6400000000|
|2015-01-21| 6.5600000000|8.2200000000|13.0000000000|

**示例**
  #data = get_price(['000300.SZ'])[:2]


```python
# 获取一支股票
    df = get_price('000001.SZ')  # 获取000001.SZ的2015年的按天数据
    df = get_price('000001.SZ', start_date='2015-01-01', end_date='2015-01-31 23:00:00',fields=['open', 'close'])# # 获得000001.XSHG的2015年01月的数据, 只获取open+close字段
    df = get_price('000001.XSHE', count = 2, end_date='2015-01-31', frequency='daily', fields=['open', 'close']) # 获取获得000001.SZ在2015年01月31日前2个交易日的数据
# 获取多只股票
    panel = get_price(get_index_stocks('000903.SZ'))#获取中证100的所有成分股的2015年的天数据, 返回一个[pandas.Panel]
    df_open = panel['open']  # 获取开盘价的[pandas.DataFrame],  行索引是[datetime.datetime]对象, 列索引是股票代号
    df_volume = panel['volume']  # 获取交易量的[pandas.DataFrame]
    df_open['000001.SZ'] # 获取平安银行的2015年每天的开盘价数据
```

## history - 获取历史数据
获取历史数据

**参数**
- param count: 数量, 返回的结果集的行数
- param unit: 单位时间长度, 几天或者几分钟, 现在支持’Xd’,’Xm’, X是一个正整数, 分别表示X天和X分钟(不论是按天还是按分钟回测都能拿到这两种单位的数据), 注意, 当X > 1时, field只支持[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段.
- param field: 要获取的数据类型, 支持SecurityUnitData里面的所有基本属性,，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’, ‘factor’, ‘high_limit’,’ low_limit’, ‘avg’, ’ pre_close’, ‘paused’]
- param security_list: 要获取数据的股票列表, None表示universe中选中的所有股票                 
- param df: 若是True, 返回pandas.DataFrame, 否则返回一个dict, 具体请看下面的返回值介绍. 默认是True.  
- param fields: 字符串list, 选择要获取的行情数据字段, 默认是None(表示[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段), 支持SecurityUnitData里面的所有基本属性,，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’, ‘factor’, ‘high_limit’,’ low_limit’, ‘avg’, ’ pre_close’, ‘paused’]
- param skip_paused: 是否跳过不交易日期(包括停牌, 未上市或者退市后的日期). 如果不跳过, 停牌时会使用停牌前的数据填充(具体请看SecurityUnitData的paused属性), 上市前或者退市后数据都为 nan, 但要注意: 
    - 默认为 False
    - 当 skip_paused 是 True 时, 只能取一只股票的信息

**返回**
    - df=True:  pandas.DataFrame对象, 行索引是datetime.datetime对象, 列索引是股票代号
    - df=False: dict, key是股票代码, 值是一个numpy数组numpy.ndarray, 对应上面的DataFrame的每一列  
     
```python
history(count, unit='1d', field='avg', security_list=None, df=True, skip_paused=False, fq='pre')
```
**回测环境/模拟专用API**

```python
h = history(5, security_list=['000001.SZ', '000002.SZ'])
```


### 结果

| | 000001.SZ  |   000002.SZ|
|--|-|--|
|2017-02-17  |20.5801000000 | 9.4428000000|
|2017-02-16  |20.6395000000 | 9.4522000000|
|2017-02-15  |20.7366000000 | 9.4733000000|
|2017-02-14  |20.8665000000 | 9.3906000000|
|2017-02-13  |20.6873000000 | 9.3982000000|

```python
h = history(5, security_list=['000001.SZ', '000002.SZ'],df =False)
```
### 结果

{

'000001.SZ': [Decimal('20.5801000000'), Decimal('20.6395000000'), Decimal('20.7366000000'), Decimal('20.8665000000'), Decimal('20.6873000000')], 
'000002.SZ': [Decimal('9.4428000000'), Decimal('9.4522000000'), Decimal('9.4733000000'), Decimal('9.3906000000'), Decimal('9.3982000000')]

}


```
###示例
h = history(5, security_list=['000001.SZ', '000002.SZ'])
h['000001.SZ'] #000001(平安银行)过去5天的每天的平均价, 一个pd.Series对象, index是datatime
h['000001.SZ'][-1] #000001(平安银行)昨天(数组最后一项)的平均价

h = history(5, security_list=['000001.XSHE', '000002.XSHE'], df=False)
h['000001.XSHE']
h['000001.XSHE'][0]
h['000001.XSHE'][-1]
```


## attribute_history  - 获取历史数据
查看某一支股票的历史数据, 可以选这只股票的多个属性, 默认跳过停牌日期.当取天数据时, 不包括当天的, 即使是在收盘后

```python
attribute_history(security, count, unit='1d',
            fields=['open', 'close', 'high', 'low', 'volume', 'money'],
            skip_paused=True, df=True, fq='pre')
```
**参数**

- security: 股票代码
- count: 数量, 返回的结果集的行数
- unit: 单位时间长度, 几天, 现在支持 ‘Xd’ X是一个正整数, 分别表示X天(不论是按天还是按分钟回测都能拿到这两种单位的数据), 注意, 当 X > 1 时, field 只支持 [‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’] 这几个标准字段.
- fields: 股票属性的list, 支持SecurityUnitData里面的所有基本属性，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’,   ‘avg’, ’ pre_close’, ‘paused’]

- skip_paused: 是否跳过不交易日期(包括停牌, 未上市或者退市后的日期). 如果不跳过, 停牌时会使用停牌前的数据填充(具体请看SecurityUnitData的paused属性), 上市前或者退市后数据都为 nan, 默认是True

- df: 若是True, 返回pandas.DataFrame, 否则返回一个dict, 具体请看下面的返回值介绍. 默认是True.我们之所以增加df参数, 是因为pandas.DataFrame创建和操作速度太慢, 很多情况并不需要使用它. 为了保持向上兼容, df默认是True, 但是如果你的回测速度很慢, 请考虑把df设成False.
```
stock = '000001.SZ'
h = attribute_history(stock, 5, '1d', ('open','close', 'volume')) # 取得000001(平安银行)过去5天的每天的开盘价, 收盘价, 交易量, 当前日期：2015-01-07
```


### df= true
 |                   | open          |close  |              volume|
 |-|-|-|-|
|2015-01-06|  15.8500000000  |15.7800000000 | 216642140.0000000000|
|2015-01-05|  15.9900000000  |16.0200000000 | 286043643.0000000000|
|2014-12-31 | 15.6100000000  |15.8400000000 | 240006571.0000000000|
|2014-12-30 | 14.9300000000  |15.5000000000 | 236607838.0000000000|
|2014-12-29 | 15.6100000000  |14.9200000000 | 258614817.0000000000|

- df=False: 
dict, key是股票代码, 值是一个numpy数组numpy.ndarray, 对应上面的DataFrame的每一列
### df= False
{\
'close': [Decimal('15.7800000000'), Decimal('16.0200000000'), Decimal('15.8400000000'), Decimal('15.5000000000'), Decimal('14.9200000000')], \
'open': [Decimal('15.8500000000'), Decimal('15.9900000000'), Decimal('15.6100000000'), Decimal('14.9300000000'), Decimal('15.6100000000')], \
'volume': [Decimal('216642140.0000000000'), Decimal('286043643.0000000000'), Decimal('240006571.0000000000'), Decimal('236607838.0000000000'), Decimal('258614817.0000000000')]\
}

## get_current_data  - 获取当前时间数据
回测环境/模拟专用API,获取当前单位时间（当天/当前分钟）的涨跌停价, 是否停牌，当天的开盘价等。

回测时, 通过 API 获取到的是前一个单位时间(天/分钟)的数据, 而有些数据, 我们在这个单位时间是知道的, 比如涨跌停价, 是否停牌, 当天的开盘价. 我们添加了这个API用来获取这些数据.

参数

现在不需要传入, 即使传入了, 返回的 dict 也是空的, dict 的 value 会按需获取.
返回值 
一个dict, 其中 key 是股票代码, value 是拥有如下属性的对象

high_limit: 涨停价
low_limit: 跌停价
paused: 是否停止或者暂停了交易, 当停牌、未上市或者退市后返回 True
is_st: 是否是 ST(包括ST, *ST)，是则返回 True，否则返回 False
day_open: 当天开盘价
name: 股票现在的名称, 可以用这个来判断股票当天是否是 ST, *ST, 是否快要退市
industry_code: 股票现在所属行业代码, 参见 行业概念数据
注意

为了加速, 返回的 dict 里面的数据是按需获取的, dict 初始是空的, 当你使用 current_data[security] 时(假设 current_data 是返回的 dict), 该 security 的数据才会被获取.
返回的结果只在当天有效, 请不要存起来到隔天再用
示例

```python
get_current_data()
```
### 使用

```python
def handle_data(context, data):
    current_data = get_current_data()
    pritn(len(current_data)
    print(type(current_data)
    print(type(current_data['000001.SZ'])
    print current_data['000001.SZ'].paused
    print current_data['000001.SZ'].open
```

4348\
<class 'dict'>\
<class 'zk_api.tables.zk_orm_tables.CHDQUOTE'>\
False\
15.5600000000



### 数据
 | date   | open |	close|	high |	low  |	volume   |	money
----------|------|-------|-------|-------|-----------|-------------
2015-01-05|	10.85|	10.87|	11.04|	10.58|	421691727|	4565387776 | 

## get_extras - 获取基金净值/期货结算价等
```python
get_extras(info, security_list, start_date='2015-01-01', end_date='2015-12-31', df=True, count=None)
```
得到多只标的在一段时间的如下额外的数据:
- is_st: 是否是ST，是则返回 True，否则返回 False
- acc_net_value: 基金累计净值
- unit_net_value: 基金单位净值
- futures_sett_price: 期货结算价
- futures_positions: 期货持仓量

注：二期将支持其它，本期仅仅支持is_st

参数
- info: [‘is_st’, ‘acc_net_value’, ‘unit_net_value’, ‘futures_sett_price’, ‘futures_positions’] 中的一个
- security_list: 股票列表
- start_date/end_date: 开始结束日期, 同 get_price
- df: 返回pandas.DataFrame对象还是一个dict, 同 history
- count: 数量, 与 start_date 二选一, 不可同时使用, 必须大于 0. 表示取 - end_date 往前的 count 个交易日的数据

返回值
- df=True: 
pandas.DataFrame对象, 列索引是股票代号, 行索引是datetime.datetime,

```python
get_extras('is_st', ['600601.SZ', '600602.SZ', '600652.SZ'], start_date='1990-12-21',end_date='1990-12-23'
```
返回:
|          |600601.SH |600602.SH|600652.SH|
|----------|----------|---------|------|
|1990-12-21|    True  |   True  | True |

- df=False 
一个dict, key是基金代号, value是list, 

```python
get_extras('is_st', ['600601.SH', '600602.SH', '600652.SH'], start_date='2006-12-21', end_date='2006-12-23',df = False)
```


返回:
```python
{
    '600601.SH': ['False', 'False'], 
    '600602.SH': ['False', 'False'], 
    '600652.SH': ['False', 'False']
}
```

## get_fundamentals - 查询财务数据 

```python
get_fundamentals(query_object, date=None, statDate=None)
```
查询财务数据，详细的数据字段描述请点击财务数据文档查看
date和statDate参数只能传入一个:

- 传入date时, 查询指定日期date收盘后所能看到的最近(对市值表来说, 最近一天, 对其他表来说, 最近一个季度)的数据, 我们会查找上市公司在这个日期之前(包括此日期)发布的数据, 不会有未来函数.
- 传入statDate时, 查询 statDate 指定的季度或者年份的财务数据. 注意: 
由于公司发布财报不及时, 一般是看不到当季度或年份的财务报表的, 回测中使用这个数据可能会有未来函数, 请注意规避.
由于估值表每天更新, 当按季度或者年份查询时, 返回季度或者年份最后一天的数据
对于年报数据, 我们目前只有现金流表和利润表, 当查询其他表时, 会返回该年份最后一个季报的数据

当 date 和 statDate 都不传入时, 相当于使用 date 参数, date 的默认值下面会描述.

参数

- query_object: 一个sqlalchemy.orm.query.Query对象(http://docs.sqlalchemy.org/en/rel_1_0/orm/query.html), 可以通过全局的 query 函数获取 Query 对象
- date: 查询日期, 一个字符串(格式类似’2015-10-15’)或者datetime.date/datetime.datetime对象, 可以是None, 使用默认日期. 这个默认日期在回测和研究模块上有点差别:

回测模块: 默认值会随着回测日期变化而变化, 等于 context.current_dt 的前一天(实际生活中我们只能看到前一天的财报和市值数据, 所以要用前一天)
研究模块: 使用平台财务数据的最新日期, 一般是昨天. 
如果传入的 date 不是交易日, 则使用这个日期之前的最近的一个交易日
- statDate: 财报统计的季度或者年份, 一个字符串, 有两种格式:

季度: 格式是: 年 + ‘q’ + 季度序号, 例如: ‘2015q1’, ‘2013q4’.
年份: 格式就是年份的数字, 例如: ‘2015’, ‘2016’.
返回 
返回一个 pandas.DataFrame, 每一行对应数据库返回的每一行(可能是几个表的联合查询结果的一行), 列索引是你查询的所有字段 
注意： 
1. 为了防止返回数据量过大, 我们每次最多返回10000行 
2. 当相关股票上市前、退市后，财务数据返回各字段为空

**示例**

```python
# 查询'600000.SH'的所有市值数据, 时间是2005-06-30  
q = query(CHDQUOTE).filter(CHDQUOTE.code == '600000.SH')
df = get_fundamentals(q, '2005-06-20')
print(df['market_cap'][0])
```

**结果**
|29597400000.000|
|---------------|


```
# 获取多只股票在某一日期的市值, 利润#VALUATION
df = get_fundamentals(query(
    CHDQUOTE , INCOME
).filter(
    CHDQUOTE.code.in_(['600010.SH','600011.SH'])
), date='2016-10-1')
print(df)
```
**结果**
 
||symbol|capitalization|market_cap|pre_close|high|circulating_market_cap|circulating_cap|paused|avg|...|operating_tax_surcharges|operating_cost|withdraw_insurance_contract_reserve|date|operating_revenue|fair_value_variable_income|total_profit|total_composite_income|ci_parent_company_owners|net_profit|
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|0|600010|3.26E+06|90518850545|2.79|2.79|43762827159|1.57E+06|FALSE|2.7775|...|5.26E+07|1.14E+10|None|2016/9/30|1.20E+10|-3946475.08|7.88E+07|1.14E+07|2.77E+07|1.11E+07|
|1|600011|1.52E+06|1.07011E+11|7.08|7.11|73920000000|1.05E+06|FALSE|7.0567|...|4.57E+08|3.78E+10|None|2016/9/30|5.29E+10|-17827952|1.02E+10|8.76E+09|7.29E+09|7.76E+09|


```python
# 查询平安银行2014年四个季度的季报, 放到数组中
    q = query(
        INCOME.date,
        INCOME.code,
        INCOME.basic_eps,
        BALANCE.cash_equivalents,
        CASH_FLOW.goods_sale_and_service_render_cash
    ).filter(
        INCOME.code == '000001.SZ',
    )

    rets = [get_fundamentals(q, statDate='2014Q' + str(i)) for i in range(1,5)]
    print(rets)
```
结果


```
[        date       code  basic_eps  cash_equivalents  
0 2014-03-31  000001.SZ       0.43      2.299240e+11   

  goods_sale_and_service_render_cash  
0                               None  ,         date       code  basic_eps  cash_equivalents  
0 2014-06-30  000001.SZ       0.53      2.581100e+11   

  goods_sale_and_service_render_cash  
0                               None  ,         date       code  basic_eps  cash_equivalents  
0 2014-09-30  000001.SZ       0.35      2.596040e+11   

  goods_sale_and_service_render_cash  
0                               None  ,         date       code  basic_eps  cash_equivalents  
0 2014-12-31  000001.SZ       0.49      2.773250e+11   

  goods_sale_and_service_render_cash  
0                               None  ]
```


```
# 查询平安银行2014年的年报
q = query(
        income.statDate,
        income.code,
        income.cinst60,
        cash_flow.goods_sale_and_service_render_cash
    ).filter(
        income.code == '000001.XSHE',
    )

ret = get_fundamentals(q, statDate='2014')
```
结果
|       | date   |    code | cinst60| goods_sale_and_service_render_cash|
|---|----|----|----|-----|
|0 |2014-12-31|  000001.SZ  |     1.73   | None|




## get_index_stocks - 获取指数成份股

```python
get_index_stocks(index_symbol, date=None)
```
获取一个指数给定日期在平台可交易的成分股列表，请点击指数列表查看指数信息

**参数**

- index_symbol: 指数代码
- date: 查询日期, 一个字符串(格式类似’2015-10-15’)或者datetime.date/datetime.datetime对象, 可以是None, 使用默认日期. 这个默认日期在回测和研究模块上有点差别: 
    1. 回测模块: 默认值会随着回测日期变化而变化, 等于context.current_dt
    1. 研究模块: 默认是今天
    
**返回**

    返回股票代码的list

**示例**

```python
stocks = get_index_stocks('000903.SZ')
print(stocks)
```
**结果**

    ['000001.SZ', '000002.SZ', '000063.SZ', '000069.SZ', '000166.SZ', '000333.SZ', '000538.SZ', '000625.SZ', '000651.SZ', '000725.SZ', '000776.SZ', '000858.SZ', '000895.SZ', '001979.SZ', '002024.SZ', '002027.SZ', '002252.SZ', '002304.SZ', '002415.SZ', '002594.SZ', '002673.SZ', '002736.SZ', '002739.SZ', '002797.SZ', '300059.SZ', '300104.SZ', '600000.SH', '600010.SH', '600015.SH', '600016.SH', '600018.SH', '600019.SH', '600023.SH', '600028.SH', '600030.SH', '600036.SH', '600048.SH', '600050.SH', '600061.SH', '600104.SH', '600111.SH', '600115.SH', '600276.SH', '600340.SH', '600485.SH', '600518.SH', '600519.SH', '600585.SH', '600606.SH', '600637.SH', '600663.SH', '600690.SH', '600705.SH', '600795.SH', '600837.SH', '600871.SH', '600886.SH', '600887.SH', '600893.SH', '600900.SH', '600958.SH', '600959.SH', '600999.SH', '601006.SH', '601018.SH', '601088.SH', '601111.SH', '601166.SH', '601169.SH', '601186.SH', '601198.SH', '601211.SH', '601225.SH', '601288.SH', '601318.SH', '601328.SH', '601336.SH', '601377.SH', '601390.SH', '601398.SH', '601600.SH', '601601.SH', '601618.SH', '601628.SH', '601633.SH', '601668.SH', '601669.SH', '601688.SH', '601727.SH', '601766.SH', '601788.SH', '601800.SH', '601818.SH', '601857.SH', '601899.SH', '601901.SH', '601985.SH', '601988.SH', '601989.SH', '601998.SH']

## get_industry_stocks - 获取行业成份股

```python
get_industry_stocks(industry_code, date=None)
```
获取在给定日期一个行业的所有股票，行业分类列表见数据页面-行业概念数据。

**参数**

- industry_code: 行业编码
- date: 查询日期, 一个字符串(格式类似’2015-10-15’)或者datetime.date/datetime.datetime对象, 可以是None, 使用默认日期. 这个默认日期在回测和研究模块上有点差别: 
    1. 回测模块: 默认值会随着回测日期变化而变化, 等于context.current_dt
    1. 研究模块: 默认是今天
**返回 **
返回股票代码的list

**示例**

```python
# 获取计算机/互联网行业的成分股
stocks = get_industry_stocks('I64')
print(stocks)
```
**结果**

    ['600652.SH', '600634.SH', '000503.SZ', '600804.SH', '002113.SZ', '002131.SZ', '300295.SZ', '002095.SZ', '002315.SZ', '603888.SH', '300052.SZ', '002354.SZ', '300031.SZ', '002175.SZ', '002174.SZ', '300113.SZ', '300226.SZ', '002555.SZ', '603000.SH', '300059.SZ', '002467.SZ', '002439.SZ', '300343.SZ', '002517.SZ', '300104.SZ', '300392.SZ', '002558.SZ', '300431.SZ', '300467.SZ', '002464.SZ', '300315.SZ', '603444.SH', '300494.SZ', '300418.SZ', '600986.SH', '603881.SH', '300242.SZ', '002624.SZ', '300571.SZ', '603258.SH']

## get_concept_stocks - 获取概念成份股

```python
get_concept_stocks(concept_code, date=None)
```
获取在给定日期一个概念板块的所有股票，概念板块分类列表见数据页面-行业概念数据。

**参数**

- concept_code: 概念板块编码
- date: 查询日期, 一个字符串(格式类似’2015-10-15’)或者datetime.date/datetime.datetime对象, 可以是None, 使用默认日期. 这个默认日期在回测和研究模块上有点差别: 
    1. 回测模块: 默认值会随着回测日期变化而变化, 等于context.current_dt
    1. 研究模块: 默认是今天

**返回** 

返回股票代码的list

**示例**

```python
# 获取概念成份股 大智慧概念代码698
    stocks = get_concept_stocks(698)
    print(stocks)
```
**结果**

    ['600801.SH', '600801.SH', '600801.SH', ... '300242.SZ', '300242.SZ', '300242.SZ', '000978.SZ']

## get_all_securities - 获取所有标的信息
获取平台支持的所有股票、基金、指数、期货信息

**参数**

- types: list: 用来过滤securities的类型, list元素可选: ‘stock’, ‘fund’, ‘index’, ‘futures’, ‘etf’, ‘lof’, ‘fja’, ‘fjb’. - - types为空时返回所有股票, 不包括基金,指数和期货
- date: 日期, 一个字符串或者 datetime.datetime/datetime.date 对象, 用于获取某日期还在上市的股票信息. 默认值为 None, 表示获取所有日期的股票信息



pandas.DataFrame, 比如:
```python
get_all_securities()[:2]
```
**返回 **

|	|end_date|name|start_date|display_name|code|
|-|-|-|-|-|-|
|000001.SZ|NaT|	PAYH|1991/4/3|平安银行|000001.SZ|
|000002.SZ|NaT|	WKA|1991/1/29|万科A|000002.SZ|


## get_security_info - 获取单个标的信息

```python
get_security_info(code)
```
获取股票/基金/指数的信息.

参数

- code: 证券代码
返回值

- 一个对象, 有如下属性:

 - display_name: 中文名称
     - name: 缩写简称
     - start_date: 上市日期, datetime.date 类型
     - end_date: 退市日期， datetime.date 类型, 
     - type: 类型，stock(股票)，index(指数)，etf(ETF基金)，fja（分级A），fjb（分级B）
     - parent: 分级基金的母基金代码
     
示例
 # 获取单个标的信息

```python
info  =  get_security_info('000789.SZ'))
print(info)
```
结果

| |display_name|code|end_date|name|start_date|type|status|
|-|-|-|-|-|-|-|-|
|000789.SZ|万年青|000789.SZ|None|WNQ|1997/9/23|stock|FALSE|

## get_all_trade_days - 获取所有交易日

```python
get_all_trade_days(start_date = '2015-01-01')
```
获取自start_date至今的所有交易日,  返回一个包含所有交易日的 numpy.ndarray, 每个元素为一个 datetime.date 类型.

**示例**

```python
arr = get_all_trade_days()
print(arr)
```
**结果**


    [
     datetime.datetime(2015, 1, 5, 0, 0) datetime.datetime(2015, 1, 6, 0, 0)
     datetime.datetime(2015, 1, 7, 0, 0) datetime.datetime(2015, 1, 8, 0, 0)
    ...
    ]

## get_trade_days - 获取指定范围交易日

```python
get_trade_days(start_date=None, end_date=None, count=None)
```
获取指定日期范围内的所有交易日, 返回 numpy.ndarray, 包含指定的 start_date 和 end_date, 默认返回至 datatime.date.today() 的所有交易日 

参数

- start_date: 开始日期, 与 count 二选一, 不可同时使用. str/datetime.date/datetime.datetime 对象
- end_date: 结束日期, str/datetime.date/datetime.datetime 对象, 默认为 datetime.date.today()
- count: 数量, 与 start_date 二选一, 不可同时使用, 必须大于 0. 表示取 end_date 往前的 count 个交易日，包含 end_date 当天。

**示例**

```python
#获取2016-12-20到2017-1-1所有交易日")
trade_days = get_trade_days(start_date='2016-12-20',end_date = '2017-1-1')
print(trade_days)
```

结果

    [datetime.datetime(2016, 12, 20, 0, 0)
    datetime.datetime(2016, 12, 21, 0, 0)
    datetime.datetime(2016, 12, 22, 0, 0)
    datetime.datetime(2016, 12, 23, 0, 0)
    datetime.datetime(2016, 12, 26, 0, 0)
    datetime.datetime(2016, 12, 27, 0, 0)
    datetime.datetime(2016, 12, 28, 0, 0)
    datetime.datetime(2016, 12, 29, 0, 0)
    datetime.datetime(2016, 12, 30, 0, 0)]
       
      
```python
#获取2017-2-1前5个交易日
trade_days = get_trade_days( end_date='2017-2-1',count= 5)
print(trade_days)
```
结果

    [datetime.date(2017, 1, 26) datetime.date(2017, 1, 25)
    datetime.date(2017, 1, 25) datetime.date(2017, 1, 25)
    datetime.date(2017, 1, 25)]

 