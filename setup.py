from setuptools import setup, find_packages

setup(
    name = "zhikuang_api",
    version = "0.1",
    packages = find_packages(),
    install_requires=[
        'cn_stock_holidays',
        'sqlalchemy',
        'pandas',
        'numpy'
    ],

    # metadata for upload to PyPI
    author="david",
    author_email="chen_mail_box@126",
    description="zhikuang api get china stock exchange data ",
    license="MIT",
    keywords="zhikuang api",

)
