#!/usr/bin/env python

# -*- coding: utf-8 -*-
from sqlalchemy import create_engine,inspect,and_,or_
import sqlalchemy

import pandas as pd

from zhikuang_api.utils.date_utils import *
from zhikuang_api.utils.user_space_utils import  *
from zhikuang_api.utils.utils import *


try:
    import zipline.api
    from zipline.utils.algo_instance import get_algo_instance, set_algo_instance
except ImportError:
    print("import zipline error!")
    pass

from cn_stock_holidays.data import *
from zhikuang_api.tables.zk_orm_tables import Base, CHDQUOTE,IDCOMPT,SECURITIES,VALUATION,INCOME,INDUSTRY_STOCKS,BALANCE,CASH_FLOW

import numpy as np


engine = create_engine('mssql+pymssql://wldata_test:!QAZ1qaz2wsx@210.51.188.104:1433/wldata_test', echo=True)
DBSession = sqlalchemy.orm.sessionmaker(bind=engine)

DEFAULT_START_DATE = '2015-01-01'
DEFAULT_END_DATE = '2015-12-31'

def query(*args):
    s = DBSession()
    return s.query(*args)



frequency_dict = {
    'daily': '1d',
    'minute': '1m',
}

def get_price(security, start_date=None, end_date=None, frequency='daily', fields=None, skip_paused=False, count=None):
    """ 获取历史数据
     :param security: 一支股票代码或者一个股票代码的list
     :param count: 与 start_date 二选一，不可同时使用. 数量, 返回的结果集的行数, 即表示获取 end_date 之前几个 frequency 的数据
     :param start_date: 与 count 二选一，不可同时使用. 字符串或者 datetime.datetime/datetime.date 对象, 开始时间. 
                        如果 count 和 start_date 参数都没有, 则 start_date 生效, 值是 ‘2015-01-01’. 注意:
                        当取分钟数据时, 时间可以精确到分钟, 比如: 传入 datetime.datetime(2015, 1, 1, 10, 0, 0) 或者 '2015-01-01 10:00:00'.
                        当取分钟数据时, 如果只传入日期, 则日内时间是当日的 00:00:00.
                        当取天数据时, 传入的日内时间会被忽略
     :param end_date: 格式同上, 结束时间, 默认是’2015-12-31’, 包含此日期. 注意: 当取分钟数据时, 如果 end_date 只有日期, 则日内时间等同于 00:00:00, 所以返回的数据是不包括 end_date 这一天的.                  
     :param frequency: 单位时间长度, 几天或者几分钟, 现在支持’Xd’,’Xm’, ‘daily’(等同于’1d’), ‘minute’(等同于’1m’), X是一个正整数, 分别表示X天和X分钟(不论是按天还是按分钟回测都能拿到这两种单位的数据), 注意, 当X > 1时, fields只支持[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段. 默认值是daily
     :param fields: 字符串list, 选择要获取的行情数据字段, 默认是None(表示[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段), 支持SecurityUnitData里面的所有基本属性,，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’, ‘factor’, ‘high_limit’,’ low_limit’, ‘avg’, ’ pre_close’, ‘paused’]
     :param skip_paused: 是否跳过不交易日期(包括停牌, 未上市或者退市后的日期). 如果不跳过, 停牌时会使用停牌前的数据填充(具体请看SecurityUnitData的paused属性), 上市前或者退市后数据都为 nan, 但要注意: 
                         默认为 False
                         当 skip_paused 是 True 时, 只能取一只股票的信息

     :return: 如果是一支股票, 则返回pandas.DataFrame对象, 行索引是datetime.datetime对象, 列索引是行情字段名字, 比如’open’/’close’
              如果是多支股票, 则返回pandas.Panel对象, 里面是很多pandas.DataFrame对象, 索引是行情字段(open/close/…), 每个pandas.DataFrame的行索引是datetime.datetime对象, 列索引是股票代号.   
     """
    try:
        if (not isinstance(security, str) and not isinstance(security, list)):
            raise Exception("security 无效参数，参数类型为字符串或列表。")

        if(isinstance(security,str)):
            security = security.split(',')

        if (count and start_date):
            raise Exception("参数count 和 start_date 不能同时使用。")

        startd = ""
        if (start_date == None):
            start_date = "2015-01-01 00:00:00"
        if (end_date == None):
            end_date = ("%s-12-31 00:00:00") % (start_date[0:4])

        if frequency in frequency_dict:
            frequency = frequency_dict.get(frequency)

        if isinstance(security, list) and skip_paused:
            raise Exception("get_price 当 skip_paused 是 True 时, 只能取一只股票的信息")

        if (fields == None):
            fields = ['open', 'close', 'high', 'low', 'volume', 'money']

        t1 = datetime.datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
        t2 = datetime.datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")

        if not (count is None or count > 0):
            raise Exception("count 参数需要大于 0 或者为 None")

        s = DBSession()
        res = None
        if frequency.endswith('d'):
            if (isinstance(count, int)):
                res = s.query(
                    CHDQUOTE.date,
                       CHDQUOTE.exchange ,
                       CHDQUOTE.symbol,
                       CHDQUOTE.code,
                       CHDQUOTE.topen.label('open'),
                       CHDQUOTE.tclose.label('close'),
                       CHDQUOTE.high,
                       CHDQUOTE.low,
                       CHDQUOTE.volume ,
                       CHDQUOTE.money ,
                       CHDQUOTE.avg,
                       CHDQUOTE.pre_close,
                       CHDQUOTE.tmstamp,
                       CHDQUOTE.paused,
                       CHDQUOTE.is_st,
                       CHDQUOTE.capitalization,
                       CHDQUOTE.circulating_cap,
                       CHDQUOTE.market_cap,
                       CHDQUOTE.circulating_market_cap,
                       CHDQUOTE.turnover_ratio).filter(and_(CHDQUOTE.code.in_(security), CHDQUOTE.date < t2)).limit(count).all()

            else:
                res = s.query(
                CHDQUOTE.date,
                   CHDQUOTE.exchange ,
                   CHDQUOTE.symbol,
                   CHDQUOTE.code,
                   CHDQUOTE.topen.label('open'),
                   CHDQUOTE.tclose.label('close'),
                   CHDQUOTE.high,
                   CHDQUOTE.low,
                   CHDQUOTE.volume ,
                   CHDQUOTE.money ,
                   CHDQUOTE.avg,
                   CHDQUOTE.pre_close,
                   CHDQUOTE.tmstamp,
                   CHDQUOTE.paused,
                   CHDQUOTE.is_st,
                   CHDQUOTE.capitalization,
                   CHDQUOTE.circulating_cap,
                   CHDQUOTE.market_cap,
                   CHDQUOTE.circulating_market_cap,
                   CHDQUOTE.turnover_ratio).filter(
                    and_(CHDQUOTE.code.in_(security), CHDQUOTE.date.between(t1, t2))).distinct().all()

            if (len(security) == 1):
                df = pd.DataFrame([[getattr(row, f) for f in fields] for row in res], [row.date for row in res], fields)
                return df
            else:
                date_index = list(set(x.date for x in res))
                date_index.sort()
                print(date_index)
                data = [[['NA' for d in range(0,len(security))] for s in range(0,len(date_index ))] for f in range(0,len(fields))]

                for row in res:
                    data[0][date_index.index(row.date)][security.index(row.code)]= row.open
                    data[1][date_index.index(row.date)][security.index(row.code)]= row.close
                    data[2][date_index.index(row.date)][security.index(row.code)]= row.high
                    data[3][date_index.index(row.date)][security.index(row.code)]= row.low
                    data[4][date_index.index(row.date)][security.index(row.code)]= row.volume
                    data[5][date_index.index(row.date)][security.index(row.code)]= row.money

                wp = pd.Panel(data, items=fields, major_axis=date_index,minor_axis=security)
                return wp
        elif frequency.endswith('m'):
            raise Exception("get_price 二期将提供minite支持")

    except Exception as e:
        print(str(e))

    return None


def history(count, unit='1d', field='avg', security_list=None, df=True, skip_paused=False, fq='pre'):
    """ 获取历史数据
     :param count: 数量, 返回的结果集的行数
     :param unit: 单位时间长度, 几天或者几分钟, 现在支持’Xd’,’Xm’, X是一个正整数, 分别表示X天和X分钟(不论是按天还是按分钟回测都能拿到这两种单位的数据), 注意, 当X > 1时, field只支持[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段.
     :param field: 要获取的数据类型, 支持SecurityUnitData里面的所有基本属性,，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’, ‘factor’, ‘high_limit’,’ low_limit’, ‘avg’, ’ pre_close’, ‘paused’]
     :param security_list: 要获取数据的股票列表, None表示universe中选中的所有股票                 
     :param df: 若是True, 返回pandas.DataFrame, 否则返回一个dict, 具体请看下面的返回值介绍. 默认是True.  
     :param fields: 字符串list, 选择要获取的行情数据字段, 默认是None(表示[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段), 支持SecurityUnitData里面的所有基本属性,，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’, ‘factor’, ‘high_limit’,’ low_limit’, ‘avg’, ’ pre_close’, ‘paused’]
     :param skip_paused: 是否跳过不交易日期(包括停牌, 未上市或者退市后的日期). 如果不跳过, 停牌时会使用停牌前的数据填充(具体请看SecurityUnitData的paused属性), 上市前或者退市后数据都为 nan, 但要注意: 
                         默认为 False
                         当 skip_paused 是 True 时, 只能取一只股票的信息

     :return: df=True:  pandas.DataFrame对象, 行索引是datetime.datetime对象, 列索引是股票代号
              df=False: dict, key是股票代码, 值是一个numpy数组numpy.ndarray, 对应上面的DataFrame的每一列  
     """
    try:
        algo_instance = get_algo_instance()

        current_date = None
        if (algo_instance):
            current_date = algo_instance.datetime
            print("in roll back env")
        else:
            print("in research env")
            current_date= datetime.date.today() - datetime.timedelta(days=1)
        current_date = convert_datetime(current_date)
        if(field not in ['open','close','low','high','volume','money','avg','pre_close','paused']):
            raise Exception("field 无效字段")

        if (isinstance(security_list, str)):
            security_list = security_list.split(',')

        s = DBSession()
        res = None

        if(count):
            res = s.query(
                   CHDQUOTE.date,
                   CHDQUOTE.exchange ,
                   CHDQUOTE.symbol,
                   CHDQUOTE.code,
                   CHDQUOTE.topen.label('open'),
                   CHDQUOTE.tclose.label('close'),
                   CHDQUOTE.high,
                   CHDQUOTE.low,
                   CHDQUOTE.volume ,
                   CHDQUOTE.money ,
                   CHDQUOTE.avg,
                   CHDQUOTE.pre_close,
                   CHDQUOTE.tmstamp,
                   CHDQUOTE.paused,
                   CHDQUOTE.is_st,
                   CHDQUOTE.capitalization,
                   CHDQUOTE.circulating_cap,
                   CHDQUOTE.market_cap,
                   CHDQUOTE.circulating_market_cap,
                   CHDQUOTE.turnover_ratio
                    ).filter(and_(CHDQUOTE.code.in_(security_list),CHDQUOTE.date<current_date)).order_by(CHDQUOTE.date.desc()).limit(count*len(security_list)).all()
        else:
            res = s.query(
                   CHDQUOTE.date,
                   CHDQUOTE.exchange ,
                   CHDQUOTE.symbol,
                   CHDQUOTE.code,
                   CHDQUOTE.topen.label('open'),
                   CHDQUOTE.tclose.label('close'),
                   CHDQUOTE.high,
                   CHDQUOTE.low,
                   CHDQUOTE.volume ,
                   CHDQUOTE.money ,
                   CHDQUOTE.avg,
                   CHDQUOTE.pre_close,
                   CHDQUOTE.tmstamp,
                   CHDQUOTE.paused,
                   CHDQUOTE.is_st,
                   CHDQUOTE.capitalization,
                   CHDQUOTE.circulating_cap,
                   CHDQUOTE.market_cap,
                   CHDQUOTE.circulating_market_cap,
                   CHDQUOTE.turnover_ratio
                    ).filter(and_(CHDQUOTE.code.in_(security_list), CHDQUOTE.date < current_date)).order_by(CHDQUOTE.date.desc()).all()
        if(len(res)):
            if (df):
                return pd.DataFrame([[getattr(res[j + i],field) for j in range(0, len(security_list))] \
                                     for i in range(0, len(res), len(security_list))], [res[i].date \
                                                                                   for i in range(0, len(res), len(security_list))], security_list)

            else:
                data = {}
                for s in security_list:
                    data[s] = []
                print(len(res))
                for i in range(0, len(res)):
                    data[security_list[i % len(security_list)]].append(getattr(res[i],field))
                return data
        return None


    except Exception as e:
        print(str(e))
        return None


def attribute_history(security, count, unit='1d',fields=['open', 'close', 'high', 'low', 'volume', 'money'],skip_paused=True, df=True, fq='pre'):
    """ 获取历史数据,查看某一支股票的历史数据, 可以选这只股票的多个属性, 默认跳过停牌日期.当取天数据时, 不包括当天的, 即使是在收盘后
     :param security: 股票代码
     :param count: 数量, 返回的结果集的行数
     :param unit: 单位时间长度, 几天或者几分钟, 现在支持 ‘Xd’, ‘Xm’, X是一个正整数, 分别表示X天和X分钟(不论是按天还是按分钟回测都能拿到这两种单位的数据), 注意, 当 X > 1 时, field 只支持 [‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’] 这几个标准字段.
     :param fields: 股票属性的list, 支持SecurityUnitData里面的所有基本属性，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’, ‘factor’, ‘high_limit’,’ low_limit’, ‘avg’, ’ pre_close’, ‘paused’]                
     :param df: 若是True, 返回pandas.DataFrame, 否则返回一个dict, 具体请看下面的返回值介绍. 默认是True.  
     :param fields: 字符串list, 选择要获取的行情数据字段, 默认是None(表示[‘open’, ‘close’, ‘high’, ‘low’, ‘volume’, ‘money’]这几个标准字段), 支持SecurityUnitData里面的所有基本属性,，包含：[‘open’, ’ close’, ‘low’, ‘high’, ‘volume’, ‘money’, ‘factor’, ‘high_limit’,’ low_limit’, ‘avg’, ’ pre_close’, ‘paused’]
     :param skip_paused: 是否跳过不交易日期(包括停牌, 未上市或者退市后的日期). 如果不跳过, 停牌时会使用停牌前的数据填充(具体请看SecurityUnitData的paused属性), 上市前或者退市后数据都为 nan, 但要注意: 
                         默认为 False
                         当 skip_paused 是 True 时, 只能取一只股票的信息

     :return: df=True:  pandas.DataFrame对象, 行索引是datetime.datetime对象, 列索引是股票代号
              df=False: dict, key是股票代码, 值是一个numpy数组numpy.ndarray, 对应上面的DataFrame的每一列  
     """
    try:
        algo_instance = get_algo_instance()

        current_date = None
        if (algo_instance):
            current_date = algo_instance.datetime
            print("in roll back env")
        else:
            print("in research env")
            yesterday = datetime.date.today() - datetime.timedelta(days=1)
            current_date = convert_datetime(yesterday)

        s = DBSession()
        res = None

        if(count):
            res = s.query( CHDQUOTE.date,
                           CHDQUOTE.exchange ,
                           CHDQUOTE.symbol,
                           CHDQUOTE.code,
                           CHDQUOTE.topen.label('open'),
                           CHDQUOTE.tclose.label('close'),
                           CHDQUOTE.high,
                           CHDQUOTE.low,
                           CHDQUOTE.volume ,
                           CHDQUOTE.money ,
                           CHDQUOTE.avg,
                           CHDQUOTE.pre_close,
                           CHDQUOTE.tmstamp,
                           CHDQUOTE.paused,
                           CHDQUOTE.is_st,
                           CHDQUOTE.capitalization,
                           CHDQUOTE.circulating_cap,
                           CHDQUOTE.market_cap,
                           CHDQUOTE.circulating_market_cap,
                           CHDQUOTE.turnover_ratio).filter(and_(CHDQUOTE.code == security,CHDQUOTE.date<current_date)).order_by(CHDQUOTE.date.desc()).limit(count).all()
        else:
            res = s.query( CHDQUOTE.date,
                           CHDQUOTE.exchange ,
                           CHDQUOTE.symbol,
                           CHDQUOTE.code,
                           CHDQUOTE.topen.label('open'),
                           CHDQUOTE.tclose.label('close'),
                           CHDQUOTE.high,
                           CHDQUOTE.low,
                           CHDQUOTE.volume ,
                           CHDQUOTE.money ,
                           CHDQUOTE.avg,
                           CHDQUOTE.pre_close,
                           CHDQUOTE.tmstamp,
                           CHDQUOTE.paused,
                           CHDQUOTE.is_st,
                           CHDQUOTE.capitalization,
                           CHDQUOTE.circulating_cap,
                           CHDQUOTE.market_cap,
                           CHDQUOTE.circulating_market_cap,
                           CHDQUOTE.turnover_ratio).filter(and_(CHDQUOTE.code == security, CHDQUOTE.date < current_date)).order_by(CHDQUOTE.date.desc()).all()
        if(len(res)):
            if (df):
                return pd.DataFrame([[float(getattr(x,field)) for field in fields] for x in res], [x.date for x in res],fields)
            else:
                data = {}
                for f in fields:
                    data[f] = []
                for r in res:
                    for f in fields:
                        data[f].append(float(getattr(r,f)))
                return data
        return None


    except Exception as e:
        print(str(e))
        return None



def get_current_data():
    """  获取当前时间数据,获取当前单位时间（当天）的涨跌停价, 是否停牌，当天的开盘价等。
         回测时, 通过 API 获取到的是前一个单位时间(天)的数据, 而有些数据, 我们在这个单位时间是知道的, 比如涨跌停价, 是否停牌, 当天的开盘价. 我们添加了这个API用来获取这些数据.
     :param 不需要传入, 即使传入了, 返回的 dict 也是空的, dict 的 value 会按需获取.
     :return: 一个dict, 其中 key 是股票代码, value 是拥有如下属性的对象
                high_limit: 涨停价
                low_limit: 跌停价
                paused: 是否停止或者暂停了交易, 当停牌、未上市或者退市后返回 True
                is_st: 是否是 ST(包括ST, *ST)，是则返回 True，否则返回 False
                day_open: 当天开盘价
                name: 股票现在的名称, 可以用这个来判断股票当天是否是 ST, *ST, 是否快要退市
                industry_code: 股票现在所属行业代码, 参见 行业概念数据
     """
    try:
        algo_instance = get_algo_instance()

        current_date = None
        if (algo_instance):
            current_date = algo_instance.datetime
            print("in roll back env")
        else:
            print("in research env")
            current_date = datetime.date.today() - datetime.timedelta(days=1)
        current_date = convert_datetime(current_date)
            #current_date = convert_datetime(datetime.date.today())
        if(not is_trading_day(current_date)):
            current_date = convert_datetime(previous_trading_day(current_date))

        #fields = ['high_limit','low_limit','paused','is_st','open','name','industry_code'] 待支持
        fields = ['paused', 'is_st', 'open']

        s = DBSession()
        print(current_date)
        res = s.query( CHDQUOTE.date,
                       CHDQUOTE.exchange ,
                       CHDQUOTE.symbol,
                       CHDQUOTE.code,
                       CHDQUOTE.topen.label('open'),
                       CHDQUOTE.tclose.label('close'),
                       CHDQUOTE.high,
                       CHDQUOTE.low,
                       CHDQUOTE.volume ,
                       CHDQUOTE.money ,
                       CHDQUOTE.avg,
                       CHDQUOTE.pre_close,
                       CHDQUOTE.tmstamp,
                       CHDQUOTE.paused,
                       CHDQUOTE.is_st,
                       CHDQUOTE.capitalization,
                       CHDQUOTE.circulating_cap,
                       CHDQUOTE.market_cap,
                       CHDQUOTE.circulating_market_cap,
                       CHDQUOTE.turnover_ratio).filter(CHDQUOTE.date==current_date).all()
        print(len(res))
        if(len(res)):
            cur_data = {}
            for r in res:
                cur_data[getattr(r,'code')] = r
            return cur_data

        return None


    except Exception as e:
        print(str(e))
        return None





def get_extras(info, security, start_date='2015-01-01', end_date='2015-12-31', df=True, count=None):
    """ 获取基金净值/期货结算价等.得到多只标的在一段时间的如下额外的数据:
                            is_st: 是否是ST，是则返回 True，否则返回 False
                            acc_net_value: 基金累计净值
                            unit_net_value: 基金单位净值
                            futures_sett_price: 期货结算价
                            futures_positions: 期货持仓量
        :param info: [‘is_st’, ‘acc_net_value’, ‘unit_net_value’, ‘futures_sett_price’, ‘futures_positions’] 中的一个
        :param security_list: 股票列表
        :param start_date/end_date: 开始结束日期, 同 get_price
        :param df: 返回pandas.DataFrame对象还是一个dict, 同 history                  
        :param count: 数量, 与 start_date 二选一, 不可同时使用, 必须大于 0. 表示取 end_date 往前的 count 个交易日的数据 
        :return: df=True: pandas.DataFrame对象, 列索引是股票代号, 行索引是datetime.datetime
                 df=False 一个dict, key是基金代号, value是numpy.ndarray   
    """
    try:
        if info not in ('is_st', 'acc_net_value', 'unit_net_value', 'futures_sett_price', 'futures_positions'):
            raise Exception("info 无效参数，参数必须为'is_st', 'acc_net_value', 'unit_net_value', 'futures_sett_price', 'futures_positions'其中一种  当前为：" + str(info))

        if (not isinstance(security, str) and not isinstance(security, list)):
            raise Exception("security 无效参数，参数类型为字符串或列表。")

        if (isinstance(security, str)):
            security = security.split(',')

        if (count and start_date):
            raise Exception("参数count 和 start_date 不能同时使用。")

        if not (count is None or count > 0):
            raise Exception("count 参数需要大于 0 或者为 None")

        start_date = convert_datetime(start_date) if start_date else convert_datetime(DEFAULT_START_DATE)
        end_date = convert_datetime(end_date) if end_date else convert_datetime(DEFAULT_END_DATE)



        if(info =="is_st"):
            s = DBSession()
            res = s.query(CHDQUOTE,CHDQUOTE.code,CHDQUOTE.date,CHDQUOTE.is_st).filter(CHDQUOTE.code.in_(security), CHDQUOTE.date.between(start_date, end_date)).distinct().all()
            if(len(res)):
                print(res[0])
                if(df):
                    return  pd.DataFrame([[(False if res[j + i].is_st == None else True) for j in range(0, len(security))] for i in range(0, len(res), len(security))], [res[i].date for i in range(0, len(res), len(security))], security)
                else:
                    data = {}
                    for s in security:
                        data[s] = []
                    print(len(res))
                    for i in range(0,len(res)):
                        data[security[i%len(security)]].append('True' if res[i].is_st else 'False')
                    return data

            else:
                return None

        elif(info == "acc_net_value"):
            raise Exception("基金累计净值为二期支持数据")
        elif(info == "unit_net_value"):
            raise Exception("基金单位净值为二期支持数据")
        elif (info == "futures_sett_price"):
            raise Exception("期货结算价为二期支持数据")
        elif (info == "futures_positions"):
            raise Exception("期货持仓量为二期支持数据")
    except Exception as e:
        print(str(e))

    return None



def get_index_stocks(index_symbol, date=None):
    """ 获取指数成份股
     :param index_symbol: 指数代码 
     :param date: 查询日期, 一个字符串(格式类似’2015-10-15’)或者datetime.date/datetime.datetime对象, 可以是None, 使用默认日期. 这个默认日期在回测和研究模块上有点差别: 
                  回测模块: 默认值会随着回测日期变化而变化, 等于context.current_dt
                  研究模块: 默认是今天
     :return: 返回股票代码的list
     """
    try:
        check_validstr(index_symbol)
        if date:
            date = convert_datetime(date)
        else:
            if(not bInBackTest):
                date = datetime.date.today()
            else:
                date = None #context.current_dt  tmp code =====================回测===================

        index,symbo = index_symbol.split('.')

        s = DBSession()
        res = s.query(IDCOMPT).filter(and_((IDCOMPT.icode == index), (IDCOMPT.updatedate <= date), or_( IDCOMPT.culldate == None , IDCOMPT.culldate == convert_datetime('1900-01-01'), IDCOMPT.culldate > date ))).all()
        print(len(res))
        return [row.code for row in res]

    except Exception as e:
        print(str(e))

    return None


def get_industry_stocks(industry_code, date=None):
    """获取行业成份股
    :param industry_code:  行业编码  
    :param date:  查询日期, 一个字符串(格式类似’2015-10-15’)或者datetime.date/datetime.datetime对象, 可以是None, 使用默认日期. 这个默认日期在回测和研究模块上有点差别: 
                回测模块: 默认值会随着回测日期变化而变化, 等于context.current_dt
                研究模块: 默认是今天
    :return: 返回股票代码的list 
    """
    check_validstr(industry_code)

    if date:
        date = convert_datetime(date)
    else:
        if (not bInBackTest):
            date = datetime.date.today()
        else:
            date = None  # context.current_dt  tmp code =====================回测===================

    try:
        s = DBSession()
        #date 待表添加相关字段在处理
        res = s.query(INDUSTRY_STOCKS).filter(INDUSTRY_STOCKS.stylecode == industry_code).all()

        return [row.code for row in res]

    except Exception as e:
        print(str(e))

    return None


def get_concept_stocks (concept_code, date=None):
    """获取概念成份股
    :param concept_code: 概念板块编码  
    :param date:  查询日期, 一个字符串(格式类似’2015-10-15’)或者datetime.date/datetime.datetime对象, 可以是None, 使用默认日期. 这个默认日期在回测和研究模块上有点差别: 
                回测模块: 默认值会随着回测日期变化而变化, 等于context.current_dt
                研究模块: 默认是今天
    :return: 返回股票代码的list 
    """
    check_validstr(concept_code)

    if date:
        date = convert_datetime(date)
    else:
        if (not bInBackTest):
            date = datetime.date.today()
        else:
            date = None  # context.current_dt  tmp code =====================回测===================

    try:
        s = DBSession()
        #date 待表添加相关字段在处理
        res = s.query(INDUSTRY_STOCKS).filter(INDUSTRY_STOCKS.style == concept_code).all()

        return [row.code for row in res]

    except Exception as e:
        print(str(e))

    return None


def get_all_securities(types=[], date=None):
    """ 获取所有标的信息
    :param types: list: 用来过滤securities的类型, list元素可选: ‘stock’, ‘fund’, ‘index’, ‘futures’, ‘etf’, ‘lof’, ‘fja’, ‘fjb’. types为空时返回所有股票, 不包括基金,指数和期货  
    :param date: date: 日期, 一个字符串或者 datetime.datetime/datetime.date 对象, 用于获取某日期还在上市的股票信息. 默认值为 None, 表示获取所有日期的股票信息
    :return: pandas.DataFrame 
    """

    try:
        if(types == []):
            types = ['stock', 'fund', 'index', 'futures', 'etf','lof','fja','fjb']
        s = DBSession()
        print(types)

        res = s.query(SECURITIES).filter(SECURITIES.type.in_(types)).all()

        if (len(res) ):
            field_lst = inspect(SECURITIES).all_orm_descriptors.keys()
            print(field_lst)
            df = pd.DataFrame([[getattr(row, f) for f in field_lst] for row in res], [row.code for row in res], field_lst)
            return df

    except Exception as e:
        print(str(e))

    return None


def get_security_info(code):
    """ 获取单个标的信息
    :param code:  证券代码 
    :return: 一个对象, 有如下属性:
                                display_name: 中文名称
                                name: 缩写简称
                                start_date: 上市日期, datetime.date 类型
                                end_date: 退市日期， datetime.date 类型, 如果没有退市则为2200-01-01
                                type: 类型，stock(股票)，index(指数)，etf(ETF基金)，fja（分级A），fjb（分级B）
                                parent: 分级基金的母基金代码
    """

    check_validstr(code)

    try:
        s = DBSession()
        res = s.query(SECURITIES).filter(SECURITIES.code == code ).all()

        if (len(res) ):
            field_lst = inspect(SECURITIES).all_orm_descriptors.keys()
            df = pd.DataFrame([[getattr(row, f) for f in field_lst] for row in res], [row.code for row in res], field_lst)
            return df

    except Exception as e:
        print(str(e))

    return None

def get_fundamentals(query_object, date=None, statDate=None):
    """
        :param query_object: 一个sqlalchemy.orm.query.Query对象  
        :param date: 查询日期, 一个字符串(格式类似’2015-10-15’) 
        :param statDate: 财报统计的季度或者年份, 一个字符串, 有两种格式:
            季度: 格式是: 年 + ‘Q’ + 季度序号, 例如: ‘2015Q1’, ‘2013Q4’.
            年份: 格式就是年份的数字, 例如: ‘2015’, ‘2016’.
        :return: 返回一个 pandas.DataFrame, 每一行对应数据库返回的每一行(可能是几个表的联合查询结果的一行),
                    列索引是你查询的所有字段 
        注意： 
            1. 为了防止返回数据量过大, 我们每次最多返回10000行 
            2. 当相关股票上市前、退市后，财务数据返回各字段为空
        """
    #参数检查
    if date and statDate:
        raise Exception('date和statDate参数只能输入一个')
    by_year = False
    if statDate:
        if(isinstance(statDate, str)):
            statDate = statDate.upper()
            if 'Q' in statDate:
                statDate = statDate.replace('Q1', '-03-31').replace('Q2', '-06-30').replace('Q3', '-09-30').replace(
                    'Q4', '-12-31')
            else:
                year = int(statDate)
                by_year = True
                statDate = '%s-12-31' % year
        elif isinstance(statDate, int):
            year = int(statDate)
            by_year = True
            statDate = '%s-12-31' % year

        statDate = date_to_datetime(statDate)
        date = statDate

    else:
        #按照date参数查询
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        yesterday = datetime.datetime.strptime(str(yesterday), '%Y-%m-%d')

        if date:
            # 不能超过昨天的
            date = min(date_to_datetime(date), yesterday)
            if (not is_trading_day(date)):
                date = date_to_datetime(date)
                date = previous_trading_day(date)
        else:
            if(not bInBackTest):
                date = yesterday
            else:
                date = yesterday#获取回测时间--------------TEMP CODE--------------

    try:
        #获取查询需显示字段列表
        fields = []
        fields_map = {}

        column_descs = query_object.column_descriptions

        table_list = list(set([x['entity'] for x in column_descs]))


        for x in column_descs:
            if (x['type'] not in table_list):  # 添加字段
                if (x['entity'] not in fields_map):
                    fields_map[x['entity']] = []
                if (x['name'] not in fields_map[x['entity']]):
                    fields_map[x['entity']].append(x['name'])
                fields.append(x['name'])
            else:

                field_lst = inspect(x['type']).all_orm_descriptors.keys()


                if (x['type'] in fields_map):

                    for it in fields_map[x['type']]:
                        field_lst.remove(it)
                    fields_map[x['type']] += field_lst
                else:
                    fields_map[x['type']] = field_lst

                fields += field_lst
        data = []
        #按照date进行查询
        # 多表操作
        if (len(table_list) > 1):
            res = query_object.filter(and_((and_((x.date == date for x in (table_list)))), and_((table_list[i].code == table_list[0].code for i in range(1, len(table_list)))))).all()

            print("[out] ------>>>>record number =",len(res))
            if (len(res)):
                # 整理数据
                #data = [getattr(r[i], j) for j in table_list[i] for i in range(0,len(table_list)) for r in res]

                if (not isinstance(x, Base)):
                    if (len(res[0]) == len(fields)):
                        df = pd.DataFrame(res)
                        df.columns = fields
                        return df


                for r in res:
                    d = []
                    for x in r:
                        if (isinstance(x, Base)):
                            for j in fields_map[type(x)]:
                                d.append(getattr(x, j))
                        else:
                            d.append(x)


                    data.append(d)
                df = pd.DataFrame(data)
                df.columns = fields
                return df
        # 单表操作date
        else:
            res = query_object.filter(table_list[0].date == date).all()
            print("[out] ------->>>>record number >", len(res))
            if(len(res)):
                if (not isinstance(res[0], Base)):
                    df = pd.DataFrame(res)
                    df.columns = fields
                    return df

                data = [[getattr(res[i], fields[j]) for j in range(0, len(fields))] for i in range(0, len(res))]
                df = pd.DataFrame(data)
                df.columns = fields

            return df

    except Exception as e:
        print(str(e))

    return None

def get_orm_query_fields(query_object):
    """
    :param query_object: 一个sqlalchemy.orm.query.Query对象  
          
     :return: 返回query_object需展示的字段 列表
     """
    try:
        fields = []
        fields_map = {}


        column_descs = query_object.column_descriptions
        table_list = list(set([x['entity'] for x in column_descs]))

        for x in column_descs:
            if (x['type'] not in table_list):#添加字段
                if (x['entity'] not in fields_map):
                    fields_map[x['entity']] = []
                if(x['name'] not in fields_map[x['entity']] ):
                    fields_map[x['entity']].append(x['name'])
                fields.append(x['name'])
            else:
                field_lst = inspect(x['type']).all_orm_descriptors.keys()


                if(x['type'] in fields_map):

                    for it in fields_map[x['type']]:
                        field_lst.remove(it)
                    fields_map[x['type']] += field_lst
                else:

                    fields_map[x['type']] = field_lst

                fields += field_lst

        return fields_map,fields
    except Exception as e:
        print("----------------------except--------------")
        print(str(e))

    return None

def get_all_trade_days():
    """
     :return: 返回一个包含所有交易日的 numpy.ndarray, 每个元素为一个 datetime.date 类型
     """
    try:
        all_trade_day = []
        day_list = getPastDays()
        for d in day_list:
            dt = convert_datetime(d)
            if is_trading_day(dt):
                all_trade_day.append(dt)
        return np.array(all_trade_day,datetime.datetime)
    except Exception as e:
        print(str(e))
    return None

def get_trade_days(start_date=None, end_date=None, count=None):
    """获取指定日期范围内的所有交易日 
     :param start_date: 开始日期, 与 count 二选一, 不可同时使用. str/datetime.date/datetime.datetime 对象
     :param end_date: 结束日期, str/datetime.date/datetime.datetime 对象, 默认为 datetime.date.today()
     :param count: 数量, 与 start_date 二选一, 不可同时使用, 必须大于 0. 表示取 end_date 往前的 count 个交易日，包含 end_date 当天。
     :return:  numpy.ndarray, 包含指定的 start_date 和 end_date, 默认返回至 datatime.date.today() 的所有交易日
     """
    try:
        all_trade_day = []
        if(start_date and count):
            raise Exception('start_date和count参数只能输入一个')
        if(start_date):
            day_list = []
            if (isinstance(start_date, datetime.date) or isinstance(start_date, datetime.datetime)):
                start_date = start_date.strftime('%Y-%m-%d')
            if (isinstance(end_date, datetime.date) or isinstance(end_date, datetime.datetime)):
                end_date = end_date.strftime('%Y-%m-%d')

            day_list = getPastDays(start_date,end_date)

            for d in day_list:
                dt = convert_datetime(d)
                if is_trading_day(dt):
                    all_trade_day.append(dt)
            return np.array(all_trade_day, datetime.datetime)
        else:
            if(end_date is None):
                end_date = datetime.datetime.strptime(time.strftime('%Y-%m-%d', time.localtime(time.time())),"%Y-%m-%d")
            d = convert_datetime(end_date)
            for i in range(0,count):
                if is_trading_day(d):
                    all_trade_day.append(d)
                else:
                    d = previous_trading_day(d)
                    all_trade_day.append(d)
                    d += datetime.timedelta(days = -1)
            return np.array(all_trade_day, datetime.datetime)


    except Exception as e:
        print(str(e))
    return None

if __name__ == '__main__':
    # test
    #获取ORM query 对象 表对象列表，表及选择字段dict，所有选择字段列表
    #getfields(query(VALUATION))

    #获取历史数据
    #data = get_price('000001.SZ')
    #data = get_price(['600602.SH'])
    #data = get_price(['600602.SH'],count= 10)
    #data = get_price(['600602.SH'], frequency= 'daily')
    #data = get_price(['600602.SH'],frequency='minute')
    #data = get_price(['600602.SH', '600603.SH', '600605.SH'],)['open'][:2]

    #data = get_price(get_index_stocks('000903.SZ')[:6])['open']
    #print(data)
    #data = get_price('000001.SZ')
    #data = get_price('000001.SZ', start_date='2015-01-01', end_date='2015-01-31 23:00:00',fields=['open', 'close'])
    #data = get_price('000001.SZ', count=2, end_date='2015-01-31', frequency='daily', fields=['open', 'close'])
    #data = get_price('000001.SZ', start_date='1991-01-01')

    #panel = get_price(get_index_stocks('000903.SZ')) # 获取中证100的所有成分股的2015年的天数据, 返回一个[pandas.Panel]
    #df_open = panel['open']  # 获取开盘价的[pandas.DataFrame],  行索引是[datetime.datetime]对象, 列索引是股票代号
    #df_volume = panel['volume']  # 获取交易量的[pandas.DataFrame]
    #print(df_open['000001.SZ'])

    # 获取单个标的信息
    #df = get_security_info('000789.SZ')
    #print(df)
    #获取基金净值/期货结算价等
    #data = get_extras('isxs_st', ['600601.SH', '600602.SH', '600652.SH'], start_date='1990-12-21',end_date='1990-12-23', df=False)
    #data = get_extras('is_st', ['600601.SH', '600602.SH', '600652.SH'], start_date='1990-12-21',end_date='1990-12-23', df=True)
    #data =get_extras('is_st', ['600601.SH', '600602.SH', '600652.SH'], start_date='2006-12-21', end_date='2006-12-23',df = False)
    #data = get_extras('acc_net_value', ['510300.SZ', '510050.SZ'], start_date='2015-12-01', end_date='2015-12-03')

    #print(df)

    # 查询'600000.SH'的所有市值数据, 时间是2005-06-30  #VALUATION
    '''q = query(CHDQUOTE).filter(CHDQUOTE.code == '600000.SH')
    df = get_fundamentals(q, '2005-06-20')
    print(df['market_cap'][0])'''

    '''q = query(CHDQUOTE.code, CHDQUOTE.market_cap).filter(CHDQUOTE.code == '600000.SH')
    df = get_fundamentals(q, '2005-06-30')
    print(df)'''

    # 获取多只股票在某一日期的市值, 利润#VALUATION
    '''df = get_fundamentals(query(
        CHDQUOTE , INCOME
    ).filter(
        CHDQUOTE.code.in_(['600010.SH','600011.SH'])
    ), date='2016-10-1')
    print(df)'''

    '''df = get_fundamentals(query(
        CHDQUOTE, INCOME
    ).filter(
        CHDQUOTE.code.in_(['600010.SH', '600011.SH'])
    ), date='2016-11-20')
    print(df)'''

    '''df = get_fundamentals(query(
        CHDQUOTE, INCOME
    ).filter(
        CHDQUOTE.code.in_(['600010.SH', '600011.SH'])
    ), date='2015-10-15')
    print(df)'''
    # 选出所有的总市值大于1000亿元, 市盈率小于10, 营业总收入大于200亿元的股票 ---待于雷添加
    '''df = get_fundamentals(query(
        VALUATION.code, VALUATION.market_cap, VALUATION.pe_ratio, INCOME.total_operating_revenue
    ).filter(
        VALUATION.market_cap > 1000,
        VALUATION.pe_ratio < 10,
        INCOME.total_operating_revenue > 2e10
    ).order_by(
        # 按市值降序排列
        VALUATION.market_cap.desc()
    ).limit(
        # 最多返回100个
        100
    ), date='2015-10-15')
    print(df)'''

    #使用 or_ 函数: 查询总市值大于1000亿元 **或者** 市盈率小于10的股票 ---待于雷添加
    '''print(get_fundamentals(query(
        VALUATION.code
    ).filter(
        or_(
            VALUATION.market_cap > 1000,
            VALUATION.pe_ratio  < 10
        )
    ),date = '2015-12-31'))'''

    # 查询平安银行2014年四个季度的季报, 放到数组中
    '''q = query(
        INCOME.date,
        INCOME.code,
        INCOME.basic_eps,
        BALANCE.cash_equivalents,
        CASH_FLOW.goods_sale_and_service_render_cash
    ).filter(
        INCOME.code == '000001.SZ',
    )

    rets = [get_fundamentals(q, statDate='2014Q' + str(i)) for i in range(1,5)]
    print(rets)'''

    # 查询平安银行2014年的年报
    '''q = query(
        INCOME.date,
        INCOME.code,
        INCOME.cinst60,
        CASH_FLOW.goods_sale_and_service_render_cash
    ).filter(
        INCOME.code == '000001.SZ',
    )

    ret = get_fundamentals(q, statDate='2014')
    print(ret)'''
    # 获取指数成份股
    #stocks = get_index_stocks('000903.SZ')
    #print(stocks)
    #data = get_index_stocks('000300.sz')
    #print(data)

    # 获取概念成份股
    #stocks = get_concept_stocks(698)
    #print(stocks)



    # 获取所有标的信息
    #df = get_all_securities()[:2]

    #print(df)


    #print(get_industry_stocks('I64'))
    #dt = convert_datetime('2017-1-1')

    #arr = get_all_trade_days(dt)
    print("获取2016-12-20到2017-1-1所有交易日")
    trade_days = get_trade_days(start_date='2016-12-20', end_date='2017-1-1')
    print(trade_days)
    print("获取2017-2-1前5个交易日")
    trade_days = get_trade_days(end_date='2017-2-1', count=5)
    print(trade_days)











